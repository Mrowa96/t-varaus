process.env.BABEL_ENV = 'test';
process.env.NODE_ENV = 'test';
process.env.PUBLIC_URL = '';

process.on('unhandledRejection', (err) => {
  throw err;
});

const jest = require('jest');
const getClientEnvironment = require('../config/env');

const { raw: env } = getClientEnvironment(process.env.PUBLIC_URL);
const argv = process.argv.slice(2);

if (env.APP_TEST_COVERAGE) {
  argv.push('--coverage');
}

argv.push('--config', './client/config/jest.config.json');

jest.run(argv);
