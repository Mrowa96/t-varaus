import {
  FILTERED_NEWS_FETCHED,
  LOAD_NEWS,
  LOAD_ONE_NEWS,
  NEWS_NOT_FOUND,
  ONE_NEWS_FETCHED,
  loadNews,
  filteredNewsFetched,
  newsNotFound,
  oneNewsFetched,
  loadOneNews,
} from "../../../src/state/actions/news";

describe('news actions', () => {
  it('should create an action for loadNews', () => {
    const filters = {
      limit: 1,
      page: 2
    };
    const expectedAction = {
      type: LOAD_NEWS,
      payload: {
        requestId: 'req',
        filters
      }
    };

    expect(loadNews('req', filters)).toEqual(expectedAction);
  });

  it('should create an action for loadOneNews', () => {
    const expectedAction = {
      type: LOAD_ONE_NEWS,
      payload: {
        id: 666,
        requestId: 'req'
      }
    };

    expect(loadOneNews(666, 'req')).toEqual(expectedAction);
  });

  it('should create an action for oneNewsFetched', () => {
    const news = {
      id: 1,
      title: 'Test'
    };
    const expectedAction = {
      type: ONE_NEWS_FETCHED,
      payload: {
        response: {
          data: news
        }
      }
    };

    expect(oneNewsFetched({ data: news })).toEqual(expectedAction);
  });

  it('should create an action for filteredNewsFetched', () => {
    const filters = {
      limit: 1,
      page: 2
    };
    const news = [
      {
        id: 1,
        title: 'Test'
      }
    ];
    const expectedAction = {
      type: FILTERED_NEWS_FETCHED,
      payload: {
        response: {
          data: news
        },
        request: {
          filters
        }
      }
    };

    expect(filteredNewsFetched({ data: news }, { filters })).toEqual(expectedAction);
  });

  it('should create an action for newsNotFound', () => {
    const filters = {
      id: 1,
    };
    const expectedAction = {
      type: NEWS_NOT_FOUND,
      payload: {
        request: {
          filters
        }
      }
    };

    expect(newsNotFound({ filters })).toEqual(expectedAction);
  });
});
