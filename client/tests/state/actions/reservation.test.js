import {
  ADD_OR_REMOVE_SEAT,
  CHANGE_TICKET_QUANTITY,
  CLEAR_RESERVATION,
  CREATE_RESERVATION,
  UPDATE_CLIENT_DATA,
  SET_RESERVATION_PARAMETERS,
  SET_RESERVATION_STEP,
  DELETE_RESERVATION,
  UPDATE_AGREEMENT,
  COMPLETE_RESERVATION,
  changeTicketQuantity,
  clearReservation,
  createReservation,
  addOrRemoveSeat,
  setReservationParameters,
  setReservationStep,
  updateClientData,
  updateAgreement,
  deleteReservation,
  completeReservation,
  RESTORE_RESERVATION,
  restoreReservation,
} from '../../../src/state/actions/reservation';
import ReservationStep from '../../../src/models/ReservationStep';

describe('reservation actions', () => {
  it('should create an action CHANGE_TICKET_QUANTITY', () => {
    const expectedAction = {
      type: CHANGE_TICKET_QUANTITY,
      payload: {
        ticketId: 1,
        quantity: 1,
      },
    };

    expect(changeTicketQuantity(1, 1)).toEqual(expectedAction);
  });

  it('should create an action SET_RESERVATION_PARAMETERS', () => {
    const expectedAction = {
      type: SET_RESERVATION_PARAMETERS,
      payload: {
        parameters: {
          eventId: 1,
          showId: 1,
          place: {
            id: 1,
            name: 'Test place',
            room: {
              id: 1,
              name: 'Test room',
            },
          },
          date: '2018-11-20',
          startTime: '20:00',
        },
      },
    };

    expect(
      setReservationParameters({
        eventId: 1,
        showId: 1,
        place: {
          id: 1,
          name: 'Test place',
          room: {
            id: 1,
            name: 'Test room',
          },
        },
        date: '2018-11-20',
        startTime: '20:00',
      }),
    ).toEqual(expectedAction);
  });

  it('should create an action CLEAR_RESERVATION', () => {
    const expectedAction = {
      type: CLEAR_RESERVATION,
      payload: {},
    };

    expect(clearReservation()).toEqual(expectedAction);
  });

  it('should create an action ADD_OR_REMOVE_SEAT', () => {
    const expectedAction = {
      type: ADD_OR_REMOVE_SEAT,
      payload: {
        column: 1,
        row: 2,
      },
    };

    expect(addOrRemoveSeat(1, 2)).toEqual(expectedAction);
  });

  it('should create an action UPDATE_CLIENT_DATA', () => {
    const expectedAction = {
      type: UPDATE_CLIENT_DATA,
      payload: {
        property: 'name',
        value: 'test',
      },
    };

    expect(updateClientData('name', 'test')).toEqual(expectedAction);
  });

  it('should create an action SET_RESERVATION_STEP', () => {
    const expectedAction = {
      type: SET_RESERVATION_STEP,
      payload: {
        step: ReservationStep.SELECT_TICKETS,
      },
    };

    expect(setReservationStep(ReservationStep.SELECT_TICKETS)).toEqual(expectedAction);
  });

  it('should create an action CREATE_RESERVATION', () => {
    const expectedAction = {
      type: CREATE_RESERVATION,
      payload: {
        showId: 1,
        startTime: '08:00',
        requestId: 'req',
      },
    };

    expect(createReservation(1, '08:00', 'req')).toEqual(expectedAction);
  });

  it('should create an action UPDATE_AGREEMENT', () => {
    const expectedAction = {
      type: UPDATE_AGREEMENT,
      payload: {
        agreement: 'dataProcessing',
        value: true,
      },
    };

    expect(updateAgreement('dataProcessing', true)).toEqual(expectedAction);
  });

  it('should create an action DELETE_RESERVATION', () => {
    const expectedAction = {
      type: DELETE_RESERVATION,
      payload: {
        reservationId: 'rid',
        requestId: 'req',
      },
    };

    expect(deleteReservation('rid', 'req')).toEqual(expectedAction);
  });

  it('should create an action COMPLETE_RESERVATION', () => {
    const expectedAction = {
      type: COMPLETE_RESERVATION,
      payload: {
        reservationId: 'rid',
        requestId: 'req',
      },
    };

    expect(completeReservation('rid', 'req')).toEqual(expectedAction);
  });

  it('should create an action RESTORE_RESERVATION', () => {
    const reservation = {
      id: 1,
      eventId: 1,
      showId: 1,
      place: {},
      date: '2019-01-01',
      startTime: '17:00',
    };
    const expectedAction = {
      type: RESTORE_RESERVATION,
      payload: {
        reservation,
      },
    };

    expect(restoreReservation(reservation)).toEqual(expectedAction);
  });
});
