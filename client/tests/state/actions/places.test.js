import { LOAD_PLACES, PLACES_FETCHED, loadPlaces, placesFetched } from "../../../src/state/actions/places";

describe('places actions', () => {
  it('should create an action PLACES_FETCHED', () => {
    const places = [
      {
        id: 1,
        title: 'Test'
      }
    ];
    const expectedAction = {
      type: PLACES_FETCHED,
      payload: {
        response: {
          data: places
        }
      }
    };

    expect(placesFetched({ data: places })).toEqual(expectedAction);
  });

  it('should create an action LOAD_PLACES', () => {
    const expectedAction = {
      type: LOAD_PLACES,
      payload: {
        requestId: 'load_places'
      }
    };

    expect(loadPlaces('load_places')).toEqual(expectedAction);
  });
});
