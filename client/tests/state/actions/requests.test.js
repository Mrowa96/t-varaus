import {
  ADD_REQUEST,
  REQUEST_GET,
  REQUEST_DELETE,
  REQUEST_POST,
  REQUEST_PUT,
  REQUEST_FAILED,
  REQUEST_SUCCEED,
  addRequest,
  requestGet,
  requestPost,
  requestFailed,
  requestSucceed,
  requestDelete,
  requestPut,
} from "../../../src/state/actions/requests";

describe('request actions', () => {
  it('should create an REQUEST_GET action', () => {
    const endpoint = 'endpoint';
    const filters = {
      limit: 1
    };
    const requestId = Symbol('r_id');
    const succeedAction = () => {
      console.log('success');
    };
    const failedAction = () => {
      console.log('failed');
    };

    const expectedAction = {
      type: REQUEST_GET,
      payload: {
        endpoint,
        filters,
        requestId,
        succeedAction,
        failedAction,
      }
    };

    expect(requestGet(endpoint, filters, requestId, succeedAction, failedAction)).toEqual(expectedAction);
  });

  it('should create an REQUEST_DELETE action', () => {
    const endpoint = 'endpoint';
    const filters = {};
    const requestId = Symbol('r_id');
    const succeedAction = () => {
      console.log('success');
    };
    const failedAction = () => {
      console.log('failed');
    };

    const expectedAction = {
      type: REQUEST_DELETE,
      payload: {
        endpoint,
        filters,
        requestId,
        succeedAction,
        failedAction,
      }
    };

    expect(requestDelete(endpoint, filters, requestId, succeedAction, failedAction)).toEqual(expectedAction);
  });

  it('should create an REQUEST_POST action', () => {
    const endpoint = 'endpoint';
    const data = {
      id: 1
    };
    const requestId = Symbol('r_id');
    const succeedAction = () => {
      console.log('success');
    };
    const failedAction = () => {
      console.log('failed');
    };

    const expectedAction = {
      type: REQUEST_POST,
      payload: {
        endpoint,
        data,
        requestId,
        succeedAction,
        failedAction,
      }
    };

    expect(requestPost(endpoint, data, requestId, succeedAction, failedAction)).toEqual(expectedAction);
  });

  it('should create an REQUEST_PUT action', () => {
    const endpoint = 'endpoint';
    const data = {
      id: 1
    };
    const requestId = Symbol('r_id');
    const succeedAction = () => {
      console.log('success');
    };
    const failedAction = () => {
      console.log('failed');
    };

    const expectedAction = {
      type: REQUEST_PUT,
      payload: {
        endpoint,
        data,
        requestId,
        succeedAction,
        failedAction,
      }
    };

    expect(requestPut(endpoint, data, requestId, succeedAction, failedAction)).toEqual(expectedAction);
  });

  it('should create an REQUEST_SUCCEED action', () => {
    const data = {
      a: 1,
      b: 2
    };
    const requestId = Symbol('r_id');
    const actionToDispatch = () => {
      console.log('success');
    };

    const expectedAction = {
      type: REQUEST_SUCCEED,
      payload: {
        response: {
          data
        },
        requestId,
        actionToDispatch
      }
    };

    expect(requestSucceed({ data }, requestId, actionToDispatch)).toEqual(expectedAction);
  });

  it('should create an REQUEST_FAILED action', () => {
    const requestId = Symbol('r_id');
    const actionToDispatch = () => {
      console.log('failure');
    };

    const expectedAction = {
      type: REQUEST_FAILED,
      payload: {
        requestId,
        actionToDispatch
      }
    };

    expect(requestFailed(requestId, actionToDispatch)).toEqual(expectedAction);
  });

  it('should create an ADD_REQUEST action', () => {
    const requestId = Symbol('r_id');
    const method = 'GET';
    const endpoint = 'endpoint';
    const filters = {
      limit: 2
    };
    const data = {
      id: 1
    };

    const expectedAction = {
      type: ADD_REQUEST,
      payload: {
        method,
        endpoint,
        filters,
        requestId,
        data
      }
    };

    expect(addRequest(requestId, method, endpoint, filters, data)).toEqual(expectedAction);
  });
});
