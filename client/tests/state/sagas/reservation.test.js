import { put, select } from 'redux-saga/effects';
import { requestDelete, requestPost, requestPut } from '../../../src/state/actions/requests';
import {
  completeReservation,
  createReservation,
  deleteReservation,
  saveReservation,
} from '../../../src/state/sagas/reservation';
import { reservationCreated } from '../../../src/state/actions/reservation';

describe('reservation saga', () => {
  it('should handle CREATE_RESERVATION action', () => {
    const generator = createReservation({
      payload: {
        showId: 6,
        startTime: '08:00',
        requestId: 'req',
      },
    });
    const data = { showId: 6, time: '08:00' };

    expect(generator.next().value).toEqual(put(requestPost('reservation', data, 'req', reservationCreated)));
  });

  it('should handle SAVE_RESERVATION action with tickets save', () => {
    const generator = saveReservation({
      payload: {
        properties: ['tickets'],
        requestId: 'req',
      },
    });
    const state = {
      reservation: {
        id: 'rid',
        tickets: [{ id: 1, quantity: 3 }],
        agreements: {
          dataProcessing: true,
        },
      },
    };
    const expectedData = {
      tickets: [{ id: 1, quantity: 3 }],
    };

    expect(generator.next().value).toEqual(select());
    expect(generator.next(state).value).toEqual(
      put(requestPut(`reservation/${state.reservation.id}`, expectedData, 'req')),
    );
  });

  it('should handle SAVE_RESERVATION action with seats save', () => {
    const generator = saveReservation({
      payload: {
        properties: ['seats'],
        requestId: 'req',
      },
    });
    const state = {
      reservation: {
        id: 'rid',
        seats: [{ column: 1, row: 3 }],
        agreements: {
          dataProcessing: true,
        },
      },
    };
    const expectedData = {
      seats: [{ column: 1, row: 3 }],
    };

    expect(generator.next().value).toEqual(select());
    expect(generator.next(state).value).toEqual(
      put(requestPut(`reservation/${state.reservation.id}`, expectedData, 'req')),
    );
  });

  it('should handle SAVE_RESERVATION action with clientData save', () => {
    const generator = saveReservation({
      payload: {
        properties: ['clientData'],
        requestId: 'req',
      },
    });
    const state = {
      reservation: {
        id: 'rid',
        clientData: {
          name: 'name',
          surname: 'surname',
          email: 'email',
          phone: 'phone',
        },
        agreements: {
          dataProcessing: true,
        },
      },
    };
    const expectedData = {
      clientData: {
        name: 'name',
        surname: 'surname',
        email: 'email',
        phone: 'phone',
      },
    };

    expect(generator.next().value).toEqual(select());
    expect(generator.next(state).value).toEqual(
      put(requestPut(`reservation/${state.reservation.id}`, expectedData, 'req')),
    );
  });

  it('should handle SAVE_RESERVATION action with processing data set to false', () => {
    const generator = saveReservation({
      payload: {
        properties: ['seats'],
        requestId: 'req',
      },
    });
    const state = {
      reservation: {
        id: 'rid',
        seats: [{ column: 1, row: 3 }],
        agreements: {
          dataProcessing: false,
        },
      },
    };
    const expectedData = {
      seats: [{ column: 1, row: 3 }],
    };

    expect(generator.next().value).toEqual(select());
    expect(generator.next(state).value).toEqual(
      put(requestPut(`reservation/${state.reservation.id}`, expectedData, 'req')),
    );
  });

  it('should handle SAVE_RESERVATION action with processing data set to true and agreements property', () => {
    const generator = saveReservation({
      payload: {
        properties: ['agreements'],
        requestId: 'req',
      },
    });
    const state = {
      reservation: {
        id: 'rid',
        clientData: {
          name: 'name',
          surname: 'surname',
          email: 'email',
          phone: 'phone',
        },
        agreements: {
          dataProcessing: true,
        },
      },
    };
    const expectedData = {
      agreements: {
        dataProcessing: true,
      },
      clientData: {
        name: 'name',
        surname: 'surname',
        email: 'email',
        phone: 'phone',
      },
    };

    expect(generator.next().value).toEqual(select());
    expect(generator.next(state).value).toEqual(
      put(requestPut(`reservation/${state.reservation.id}`, expectedData, 'req')),
    );
  });

  it('should handle DELETE_RESERVATION action', () => {
    const generator = deleteReservation({
      payload: {
        reservationId: 'rid',
        requestId: 'req',
      },
    });

    expect(generator.next().value).toEqual(put(requestDelete('reservation/rid', {}, 'req')));
  });

  it('should handle COMPLETE_RESERVATION action', () => {
    const generator = completeReservation({
      payload: {
        reservationId: 'rid',
        requestId: 'req',
      },
    });

    expect(generator.next().value).toEqual(put(requestPut('reservation/rid', { complete: true }, 'req')));
  });
});
