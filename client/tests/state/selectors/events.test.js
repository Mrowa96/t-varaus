import {
  getEventFromReservation,
  getEventsByFilter,
  getEventsByFilterForDate,
  getNotEmptyFilters,
  getShowsGroupedByPlace,
} from '../../../src/state/selectors/events';
import { serializeFilters } from '../../../src/utils/serializeFilters';

describe('events selectors', () => {
  it('[getEventsByFilter] should return events as array from filtered state', () => {
    const state = {
      events: {
        byId: {
          1: {
            id: 1,
            title: 'Test',
            shows: [],
          },
          2: {
            id: 2,
            title: 'Test 2',
            shows: [],
          },
        },
        dataByFilter: {
          [serializeFilters({ placeId: 1 })]: {
            ids: [1, 2],
            totalQuantity: 2,
          },
        },
        filters: {
          placeId: 1,
        },
      },
    };
    const expectedValue = [
      {
        id: 1,
        title: 'Test',
        shows: [],
      },
      {
        id: 2,
        title: 'Test 2',
        shows: [],
      },
    ];

    expect(getEventsByFilter(state.events, { placeId: 1 })).toEqual(expectedValue);
  });

  it('[getEventsByFilter] should return undefined from filtered state if filter was not used', () => {
    const state = {
      events: {
        byId: {},
        dataByFilter: {},
        filters: {
          placeId: 1,
        },
      },
    };

    expect(getEventsByFilter(state.events, { placeId: 1 })).toEqual(undefined);
  });

  it('[getEventsByFilterForDate] should return only events for concrete date', () => {
    const filters = {
      placeId: 1,
      date: '2018-11-28',
    };
    const state = {
      events: {
        byId: {
          1: {
            id: 1,
            title: 'First event',
            shows: [
              {
                id: 1,
                date: '2018-11-27',
                startTimes: ['11:00', '13:00', '20:00'],
              },
              {
                id: 2,
                date: '2018-11-28',
                startTimes: ['11:00', '13:00', '19:00'],
              },
              {
                id: 3,
                date: '2018-11-29',
                startTimes: ['11:00', '13:00', '20:00'],
              },
            ],
          },
        },
        dataByFilter: {
          [serializeFilters(filters)]: {
            ids: [1],
            totalQuantity: 1,
          },
        },
      },
    };

    expect(getEventsByFilterForDate(state.events, filters, '2018-11-29 19:21').length).toEqual(1);
    expect(getEventsByFilterForDate(state.events, filters, '2018-11-28 19:21').length).toEqual(0);
  });

  it('[getEventFromReservation] should return event for reservations', () => {
    const state = {
      events: {
        byId: {
          1: {
            id: 1,
            title: 'Test',
            shows: [],
          },
        },
      },
      reservation: {
        eventId: 1,
      },
    };
    const expectedValue = {
      id: 1,
      title: 'Test',
      shows: [],
    };

    expect(getEventFromReservation(state)).toEqual(expectedValue);
  });

  it('[getNotEmptyFilters] should return undefined value', () => {
    const filters = getNotEmptyFilters({ events: { filters: {} } });

    expect(filters).toEqual(undefined);
  });

  it('[getNotEmptyFilters] should return object with placeId filter', () => {
    const filters = getNotEmptyFilters({ events: { filters: { placeId: 1 } } });

    expect(filters).toEqual({ placeId: 1 });
  });

  it('[getShowsGroupedByPlace] should return array with correct data', () => {
    const event = {
      id: 1,
      shows: [
        {
          id: 1,
          place: {
            id: 1,
            name: 'Test',
          },
          date: '2018-11-20',
          startTimes: ['11:00', '13:00'],
        },
        {
          id: 2,
          place: {
            id: 1,
            name: 'Test',
          },
          date: '2018-11-21',
          startTimes: ['11:00', '13:00'],
        },
        {
          id: 3,
          place: {
            id: 2,
            name: 'Test',
          },
          date: '2018-11-21',
          startTimes: ['11:00', '13:00'],
        },
      ],
    };
    const expectedValue = [
      {
        id: 1,
        name: 'Test',
        shows: [
          {
            id: 1,
            date: '2018-11-20',
            startTimes: ['11:00', '13:00'],
          },
          {
            id: 2,
            date: '2018-11-21',
            startTimes: ['11:00', '13:00'],
          },
        ],
      },
      {
        id: 2,
        name: 'Test',
        shows: [
          {
            id: 3,
            date: '2018-11-21',
            startTimes: ['11:00', '13:00'],
          },
        ],
      },
    ];

    expect(getShowsGroupedByPlace(event)).toEqual(expectedValue);
  });

  it('[getShowsGroupedByPlace] should array even if shows are undefined', () => {
    const event = { id: 1 };
    const expectedValue = [];

    expect(getShowsGroupedByPlace(event)).toEqual(expectedValue);
  });
});
