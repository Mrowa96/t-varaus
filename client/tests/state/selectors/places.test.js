import { getPlaceFromStateById, getPlacesFromState } from "../../../src/state/selectors/places";

describe('places selectors', () => {
  it('[getPlacesFromState] should return places as array if there are some places', () => {
    const state = {
      places: {
        byId: {
          1: {
            id: 1,
            name: 'Test place'
          }
        }
      }
    };
    const expectedValue = {
      1: {
        id: 1,
        name: 'Test place'
      }
    };

    expect(getPlacesFromState(state)).toEqual(expectedValue)
  });

  it('[getPlacesFromState] should return undefined value if are no places', () => {
    const state = {
      places: {
        byId: null
      },
    };

    expect(getPlacesFromState(state)).toEqual(null)
  });

  it('[getPlaceFromStateById] should return place by id', () => {
    const state = {
      places: {
        byId: {
          1: {
            id: 1,
            name: 'Test place'
          }
        }
      },
    };
    const expectedValue = {
      id: 1,
      name: 'Test place'
    };

    expect(getPlaceFromStateById(state, 1)).toEqual(expectedValue)
  });

  it('[getPlaceFromStateById] should return undefined value if place does not exists', () => {
    const state = {
      places: {
        byId: {
          2: {
            id: 2,
            name: 'Test place'
          }
        }
      },
    };

    expect(getPlaceFromStateById(state, 1)).toEqual(undefined)
  });

  it('[getPlaceFromStateById] should return undefined value if places are not loaded', () => {
    const state = {
      places: {
        byId: null
      },
    };

    expect(getPlaceFromStateById(state, 1)).toEqual(undefined)
  });
});
