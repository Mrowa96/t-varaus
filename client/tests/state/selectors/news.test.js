import { serializeFilters } from '../../../src/utils/serializeFilters';
import { getNewsByFilter, getNewsTotalQuantityByFilter, getOneNewsById } from '../../../src/state/selectors/news';

describe('news selectors', () => {
  it('should return news array by filter', () => {
    const filters = { limit: 2, page: 3 };
    const state = {
      news: {
        byId: {
          1: {
            id: 1,
            title: 'Test',
          },
          2: {
            id: 2,
            title: 'Test 2',
          },
        },
        dataByFilter: {
          [serializeFilters(filters)]: {
            ids: [1, 2],
            totalQuantity: 2,
          },
        },
      },
    };
    const expectedValue = [
      {
        id: 1,
        title: 'Test',
      },
      {
        id: 2,
        title: 'Test 2',
      },
    ];

    expect(getNewsByFilter(state.news, filters)).toEqual(expectedValue);
  });

  it('should return news total quantity by filter', () => {
    const filters = { limit: 2, page: 3 };
    const state = {
      news: {
        byId: {
          1: {
            id: 1,
            title: 'Test',
          },
        },
        dataByFilter: {
          [serializeFilters(filters)]: {
            ids: [1],
            totalQuantity: 1,
          },
        },
      },
    };

    expect(getNewsTotalQuantityByFilter(state.news, filters)).toEqual(1);
  });

  it('should return one news byId', () => {
    const state = {
      news: {
        byId: {
          1: {
            id: 1,
            title: 'Test',
          },
        },
      },
    };
    const expectedValue = {
      id: 1,
      title: 'Test',
    };

    expect(getOneNewsById(state.news, 1)).toEqual(expectedValue);
  });
});
