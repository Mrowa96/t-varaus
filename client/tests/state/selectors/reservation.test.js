import {
  getGroupedSeatsFromReservation,
  getPlaceFromReservation,
  getRoomFromReservation,
  getTotalCost,
  getTotalTicketsQuantity,
} from '../../../src/state/selectors/reservation';

describe('reservation selectors', () => {
  it('should return room from reservation place', () => {
    const state = {
      reservation: {
        eventId: 1,
        showId: 1,
        place: {
          id: 1,
          name: 'Test place',
          room: {
            id: 1,
            name: 'Test room',
            seats: {
              columns: 5,
              rows: 6,
            },
          },
        },
      },
    };
    const expectedValue = {
      id: 1,
      name: 'Test room',
      seats: {
        columns: 5,
        rows: 6,
      },
    };

    expect(getRoomFromReservation(state)).toEqual(expectedValue);
  });

  it('should return place from reservation', () => {
    const state = {
      reservation: {
        eventId: 1,
        showId: 2,
        place: {
          id: 1,
          name: 'Test place',
        },
      },
    };
    const expectedValue = {
      id: 1,
      name: 'Test place',
    };

    expect(getPlaceFromReservation(state)).toEqual(expectedValue);
  });

  it('should return total cost of reservation', () => {
    const state = {
      reservation: {
        tickets: [
          {
            id: 1,
            quantity: 3,
          },
          {
            id: 2,
            quantity: 2,
          },
        ],
      },
      tickets: {
        byId: {
          1: {
            name: 'Ticket',
            price: 19.99,
          },
          2: {
            name: 'Ticket 2',
            price: 29.44,
          },
        },
      },
    };

    expect(getTotalCost(state)).toEqual(118.85);
  });

  it('should return total quantity of tickets in reservation', () => {
    const state = {
      reservation: {
        tickets: [
          {
            id: 1,
            quantity: 3,
          },
          {
            id: 2,
            quantity: 2,
          },
        ],
      },
    };

    expect(getTotalTicketsQuantity(state)).toEqual(5);
  });

  it('should return grouped seats by row', () => {
    const state = {
      reservation: {
        seats: [
          {
            row: 6,
            column: 2,
          },
          {
            row: 6,
            column: 10,
          },
          {
            row: 1,
            column: 3,
          },
          {
            row: 1,
            column: 2,
          },
          {
            row: 1,
            column: 1,
          },
          {
            row: 2,
            column: 2,
          },
        ],
      },
    };
    const expectedValue = [
      {
        row: 1,
        columns: [1, 2, 3],
      },
      {
        row: 2,
        columns: [2],
      },
      {
        row: 6,
        columns: [2, 10],
      },
    ];

    expect(getGroupedSeatsFromReservation(state)).toEqual(expectedValue);
  });
});
