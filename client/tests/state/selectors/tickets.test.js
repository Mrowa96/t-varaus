import { getTicketsFromState, hasTickets } from "../../../src/state/selectors/tickets";

describe('tickets selectors', () => {
  it('should return tickets as array if there are some tickets', () => {
    const state = {
      tickets: {
        byId: {
          1: {
            id: 1,
            name: 'Test ticket'
          }
        }
      }
    };
    const expectedValue = [
      {
        id: 1,
        name: 'Test ticket'
      }
    ];

    expect(getTicketsFromState(state)).toEqual(expectedValue)
  });

  it('should return undefined value if are no tickets', () => {
    const state = {
      tickets: {
        byId: null
      },
    };

    expect(getTicketsFromState(state)).toEqual(undefined)
  });

  it('[hasTickets] should return false value if tickets are null', () => {
    const state = {
      tickets: {
        byId: null
      },
    };

    expect(hasTickets(state)).toEqual(false)
  });

  it('[hasTickets] should return true value if tickets are empty object', () => {
    const state = {
      tickets: {
        byId: {}
      },
    };

    expect(hasTickets(state)).toEqual(true)
  });

  it('[hasTickets] should return true value if tickets exists', () => {
    const state = {
      tickets: {
        byId: [
          {
            id: 1,
            name: 'ticket'
          }
        ]
      },
    };

    expect(hasTickets(state)).toEqual(true)
  });
});
