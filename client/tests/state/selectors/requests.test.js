import RequestStatus from '../../../src/models/RequestStatus';
import { getRequestById } from '../../../src/state/selectors/requests';

describe('requests selectors', () => {
  it('should return concrete request if exists', () => {
    const requestId = Symbol('r');
    const request = {
      endpoint: '/test',
      method: 'GET',
      filters: {},
      status: RequestStatus.SUCCESS,
    };
    const state = {
      requests: {
        [requestId]: request,
      },
    };

    expect(getRequestById(state.requests, requestId)).toEqual(request);
  });

  it('should return object with status if request not exist', () => {
    const requestId = Symbol('r');
    const state = {
      requests: {},
    };
    const expectedValue = {
      status: RequestStatus.NOT_INITIALIZED,
    };

    expect(getRequestById(state.requests, requestId)).toEqual(expectedValue);
  });
});
