import reducers from '../../../src/state/reducers/index';

describe('tickets reducers', () => {
  it('should handle TICKETS_FETCHED action if the are tickets', () => {
    const state = reducers(
        { tickets: { byId: null } },
        {
          type: 'TICKETS_FETCHED',
          payload: {
            response: {
              data: [
                {
                  id: 1,
                  name: 'Standard',
                },
                {
                  id: 2,
                  name: 'With discount',
                },
              ],
            },
          },
        },
      ),
      expectedState = {
        byId: {
          1: {
            id: 1,
            name: 'Standard',
          },
          2: {
            id: 2,
            name: 'With discount',
          },
        },
      };

    expect(state.tickets).toEqual(expectedState);
  });

  it('should handle TICKETS_FETCHED action if there are not any tickets', () => {
    const state = reducers(
        { tickets: { byId: null } },
        {
          type: 'TICKETS_FETCHED',
          payload: {
            response: {
              data: [],
            },
          },
        },
      ),
      expectedState = {
        byId: {},
      };

    expect(state.tickets).toEqual(expectedState);
  });
});
