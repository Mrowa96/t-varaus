import reducers from '../../../src/state/reducers/index';

describe('news reducers', () => {
  it('should handle FILTERED_NEWS_FETCHED action', () => {
    const state = reducers(
        { news: { byId: {}, dataByFilter: {} } },
        {
          type: 'FILTERED_NEWS_FETCHED',
          payload: {
            response: {
              data: [
                {
                  id: 1,
                  name: 'Test 1',
                },
                {
                  id: 2,
                  name: 'Test 2',
                },
              ],
              headers: {
                'x-total-count': 2,
              },
            },
            request: {
              filters: {
                page: 1,
                limit: 2,
              },
            },
          },
        },
      ),
      expectedState = {
        byId: {
          1: {
            id: 1,
            name: 'Test 1',
          },
          2: {
            id: 2,
            name: 'Test 2',
          },
        },
        dataByFilter: {
          f_page_1_limit_2_: {
            ids: [1, 2],
            totalQuantity: 2,
          },
        },
      };

    expect(state.news).toEqual(expectedState);
  });
});
