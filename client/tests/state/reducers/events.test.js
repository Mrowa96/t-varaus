import reducers from '../../../src/state/reducers/index';
import { FILTERED_EVENTS_FETCHED } from '../../../src/state/actions/events';
import { serializeFilters } from '../../../src/utils/serializeFilters';

describe('events reducers', () => {
  it('should handle FILTERED_EVENTS_FETCHED action', () => {
    const state = reducers(
        { events: { byId: {}, dataByFilter: {}, lastSeenIds: [], filters: {} } },
        {
          type: FILTERED_EVENTS_FETCHED,
          payload: {
            response: {
              data: [
                {
                  id: 1,
                  title: 'Test',
                },
                {
                  id: 2,
                  title: 'Test 2',
                },
              ],
              headers: {
                'x-total-count': 2,
              },
            },
            request: {
              filters: {
                limit: 1,
              },
            },
          },
        },
      ),
      expectedEventsState = {
        byId: {
          1: {
            id: 1,
            title: 'Test',
          },
          2: {
            id: 2,
            title: 'Test 2',
          },
        },
        dataByFilter: {
          [serializeFilters({ limit: 1 })]: {
            ids: [1, 2],
            totalQuantity: 2,
          },
        },
        lastSeenIds: [],
        filters: {},
      };

    expect(state.events).toEqual(expectedEventsState);
  });

  it('should handle EVENT_FETCHED action', () => {
    const state = reducers(
        { events: { byId: {}, lastSeenIds: [], filters: {} } },
        {
          type: 'EVENT_FETCHED',
          payload: {
            response: {
              data: {
                id: 1,
                title: 'Test',
              },
            },
          },
        },
      ),
      expectedEventsState = {
        byId: {
          1: {
            id: 1,
            title: 'Test',
          },
        },
        lastSeenIds: [1],
        filters: {},
      };

    expect(state.events).toEqual(expectedEventsState);
  });

  it('should handle SET_EVENTS_PLACE_ID_FILTER action', () => {
    const state = reducers(
        { events: { filters: { placeId: null, date: null } } },
        {
          type: 'SET_EVENTS_PLACE_ID_FILTER',
          payload: {
            placeId: 1,
          },
        },
      ),
      expectedState = {
        placeId: 1,
        date: null,
      };

    expect(state.events.filters).toEqual(expectedState);
  });

  it('should handle SET_EVENTS_DATE_FILTER action', () => {
    const state = reducers(
        { events: { filters: { placeId: null, date: null } } },
        {
          type: 'SET_EVENTS_DATE_FILTER',
          payload: {
            date: '10/18/2018',
          },
        },
      ),
      expectedState = {
        placeId: null,
        date: '10/18/2018',
      };

    expect(state.events.filters).toEqual(expectedState);
  });
});
