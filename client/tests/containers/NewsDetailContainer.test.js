import React from 'react';
import { mount } from 'enzyme';
import { createMockStore } from 'redux-test-utils';
import { LOAD_ONE_NEWS } from '../../src/state/actions/news';
import { NewsDetailConnected } from '../../src/containers/NewsDetailContainer';
import Loader from '../../src/components/Loader/Loader';
import NewsDetail from '../../src/components/NewsDetail/NewsDetail';

describe('NewsDetailContainer', () => {
  let store, component;
  const news = {
    id: 1,
    title: 'Test news',
    photo: 'photo url',
    content: 'Some content',
  };

  beforeEach(() => {
    store = createMockStore({
      news: {
        byId: {},
      },
    });

    component = mount(<NewsDetailConnected match={{ params: { id: news.id } }} />, { context: { store } });
  });

  it('should not load news if it is in store', () => {
    store = createMockStore({
      news: {
        byId: {
          [news.id]: news,
        },
      },
    });
    component = mount(<NewsDetailConnected match={{ params: { id: news.id } }} />, { context: { store } });

    expect(store.isActionTypeDispatched(LOAD_ONE_NEWS)).toBeFalsy();
  });

  it('should load news if it is not in store', () => {
    expect(store.isActionTypeDispatched(LOAD_ONE_NEWS)).toBeTruthy();
  });

  it('should display loader if news is not loaded', () => {
    expect(component.find(Loader)).toExist();
    expect(component.find(NewsDetail)).not.toExist();
  });

  it('should display NewsDetail if news is loaded and not empty', () => {
    store = createMockStore({
      news: {
        byId: {
          [news.id]: news,
        },
      },
    });
    component = mount(<NewsDetailConnected match={{ params: { id: news.id } }} />, { context: { store } });

    expect(component.find(NewsDetail)).toExist();
    expect(component.find(Loader)).not.toExist();
  });

  it('should display NewsDetail if news is loaded and empty', () => {
    store = createMockStore({
      news: {
        byId: {
          [news.id]: {},
        },
      },
    });
    component = mount(<NewsDetailConnected match={{ params: { id: news.id } }} />, { context: { store } });

    expect(component.find(NewsDetail)).toExist();
    expect(component.find(Loader)).not.toExist();
  });
});
