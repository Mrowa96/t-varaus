import React from 'react';
import { shallow } from 'enzyme';
import NewsDetail from '../../../src/components/NewsDetail/NewsDetail';
import styles from '../../../src/components/NewsDetail/NewsDetail.module.scss';
import { Helmet } from 'react-helmet';

describe('<NewsDetail/>', () => {
  let component, news;

  beforeEach(() => {
    news = {
      id: 1,
      title: 'Test news',
      category: {
        name: 'Test category',
      },
      content: '<p>Some content</p>',
      author: {
        name: 'John Smith',
      },
      photo: 'https://www.myagecalculator.com/images/scarlett-johansson.jpg',
    };
    component = shallow(<NewsDetail news={news} />);
  });

  it('renders message that news is not defined', () => {
    component = shallow(<NewsDetail />);

    expect(component.find(`.${styles.notFoundMessage}`)).toExist();
  });

  it('renders Helmet if updateMeta is true', () => {
    expect(component.find(Helmet)).toExist();
  });

  it('does not render Helmet if updateMeta is false', () => {
    component = shallow(<NewsDetail news={news} updateMeta={false} />);

    expect(component.find(Helmet)).not.toExist();
  });

  it('renders news title in h1 tag', () => {
    expect(component.find('h1.title')).toExist();
    expect(component.find('h1.title').text()).toEqual(news.title);
  });

  it('renders news content', () => {
    expect(
      component
        .find(`.${styles.content}`)
        .html()
        .includes(news.content),
    ).toBeTruthy();
  });

  it('renders news author', () => {
    expect(component.find(`.${styles.author}`).text()).toEqual(`Author: ${news.author.name}`);
  });

  it('does not render news author if not included', () => {
    delete news.author;
    component = shallow(<NewsDetail news={news} />);
    expect(component.find(`.${styles.author}`)).not.toExist();
  });

  it('renders news category name', () => {
    expect(component.find(`.${styles.category}`).text()).toEqual(news.category.name);
  });

  it('does not render news category name if not included', () => {
    delete news.category;
    component = shallow(<NewsDetail news={news} />);
    expect(component.find(`.${styles.category}`)).not.toExist();
  });

  it('renders news photo', () => {
    let photo = component.find(`.${styles.photoAndCategoryWrapper} img`);

    expect(photo.prop('src')).toEqual(news.photo);
    expect(photo.prop('alt')).toEqual(news.title);
  });
});
