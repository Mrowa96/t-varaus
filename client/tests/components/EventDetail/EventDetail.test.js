import React from 'react';
import { shallow } from 'enzyme';
import EventDetail from '../../../src/components/EventDetail/EventDetail';
import styles from '../../../src/components/EventDetail/EventDetail.module.scss';
import { Helmet } from 'react-helmet';
import EventDetailsAndDescription from '../../../src/components/EventDetail/EventDetailsAndDescription/EventDetailsAndDescription';
import EventReservations from '../../../src/components/EventDetail/EventReservations/EventReservations';

describe('<EventDetail/>', () => {
  let component, event;

  beforeEach(() => {
    event = {
      id: 1,
      title: 'Test news',
      trailerUrl: 'link-to-youtube',
      description: 'Test description',
      shows: [],
    };
    component = shallow(<EventDetail event={event} />);
  });

  it('should render message that event is not defined', () => {
    component = shallow(<EventDetail />);

    expect(component.find(`.${styles.notFoundMessage}`)).toExist();
  });

  it('should render Helmet if updateMeta is true', () => {
    expect(component.find(Helmet)).toExist();
  });

  it('should not render Helmet if updateMeta is false', () => {
    component = shallow(<EventDetail event={event} updateMeta={false} />);

    expect(component.find(Helmet)).not.toExist();
  });

  it('should render youtube iframe with correct src', () => {
    expect(component.find(`iframe[src^="${event.trailerUrl}"]`)).toExist();
  });

  it('should render EventDetailsAndDescription component', () => {
    expect(component.find(EventDetailsAndDescription)).toExist();
  });

  it('should render EventReservations component', () => {
    expect(component.find(EventReservations)).toExist();
  });
});
