import React from 'react';
import { shallow } from 'enzyme';
import Button from '../../../src/components/Button/Button';

describe('<Button/>', () => {
  let component, onClick, button;

  beforeEach(() => {
    onClick = jest.fn();
    component = shallow(<Button onClick={onClick} />);
    button = component.find('button');
  });

  it('invokes onClick method on click', () => {
    button.simulate('click');

    expect(onClick.mock.calls.length === 1).toBeTruthy();
  });

  it('renders passed text inside component', () => {
    component.setProps({ text: 'text' });
    button = component.find('button');

    expect(button.text()).toEqual('text');
  });

  it('can be disabled', () => {
    component.setProps({ disabled: true });
    button = component.find('button');

    expect(button.prop('disabled')).toEqual(true);
    //TODO Add click test
  });

  it('renders passed children inside component', () => {
    component = shallow(
      <Button text={'text test'} onClick={onClick}>
        <p>
          <span>text from child</span>
        </p>
      </Button>,
    );
    button = component.find('button');

    expect(button.html().includes('<p><span>text from child</span></p>')).toBeTruthy();
  });
});
