import React from 'react';
import { shallow } from 'enzyme';
import Loader from '../../../../src/components/Loader/Loader';
import Restore from '../../../../src/components/Reservation/Restore/Restore';

describe('<Restore/>', () => {
  let component;

  beforeEach(() => {
    component = shallow(<Restore />);
  });

  it('should have form Loader', () => {
    expect(component.find(Loader) !== null).toBeTruthy();
  });

  it('should have text message', () => {
    expect(component.find('span').text()).toEqual('We are restoring your reservation...');
  });
});
