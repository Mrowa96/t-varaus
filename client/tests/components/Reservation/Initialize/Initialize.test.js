import React from 'react';
import { shallow } from 'enzyme';
import Initialize from '../../../../src/components/Reservation/Initialize/Initialize';
import Loader from '../../../../src/components/Loader/Loader';

describe('<Initialize/>', () => {
  let component;

  beforeEach(() => {
    component = shallow(<Initialize />);
  });

  it('should have form Loader', () => {
    expect(component.find(Loader) !== null).toBeTruthy();
  });

  it('should have text message', () => {
    expect(component.find('span').text()).toEqual('We are creating your reservation...');
  });
});
