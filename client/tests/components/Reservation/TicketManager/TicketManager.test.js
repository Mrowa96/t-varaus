import React from 'react';
import { shallow } from 'enzyme';
import TicketManager from '../../../../src/components/Reservation/TicketManager/TicketManager';
import styles from '../../../../src/components/Reservation/TicketManager/TicketManager.module.scss';
import Button from '../../../../src/components/Button/Button';

describe('<TicketManager/>', () => {
  let component, event, tickets, selectedTickets, onNextButtonClick, onPrevButtonClick, onChange;

  beforeEach(() => {
    onNextButtonClick = jest.fn();
    onPrevButtonClick = jest.fn();
    onChange = jest.fn();
    tickets = [
      {
        id: 1,
        name: 'First ticket',
        price: 20,
      },
      {
        id: 2,
        name: 'Second ticket',
        price: 30,
      },
    ];
    selectedTickets = [
      {
        id: 1,
        quantity: 5,
      },
    ];
    event = {
      title: 'Test news',
      trailerUrl: 'link-to-youtube',
      description: 'Test description',
      places: [],
    };
    component = shallow(
      <TicketManager
        tickets={tickets}
        onNextButtonClick={onNextButtonClick}
        onPrevButtonClick={onPrevButtonClick}
        onChange={onChange}
        selectedTickets={selectedTickets}
      />,
    );
  });

  it('should render message that are no tickets', () => {
    component = shallow(
      <TicketManager
        tickets={[]}
        onNextButtonClick={onNextButtonClick}
        onPrevButtonClick={onPrevButtonClick}
        onChange={onChange}
        selectedTickets={[]}
      />,
    );

    expect(component.find(`.${styles.noTickets}`)).toExist();
  });

  it('should render correct number of tickets', () => {
    expect(component.find(`.${styles.ticket}`).length).toEqual(2);
  });

  it('should render ticket name, price and quantity select', () => {
    let element = component.find(`.${styles.ticket}`).first();

    expect(element.find(`.${styles.name}`).text()).toEqual(`${tickets[0].name} ticket`);
    expect(element.find(`.${styles.price}`).text()).toEqual(`${tickets[0].price} PLN`);
    expect(element.find('select')).toExist();
  });

  it('should dispatch onChange function when select changes', () => {
    component
      .find('select')
      .first()
      .simulate('change', { target: { value: 2 } });

    expect(onChange.mock.calls.length).toEqual(1);
    expect(onChange.mock.calls[0][0]).toEqual(1);
    expect(onChange.mock.calls[0][1]).toEqual(2);
  });

  it('should render correct number of ticket quantities', () => {
    component = shallow(
      <TicketManager
        tickets={tickets}
        onNextButtonClick={onNextButtonClick}
        onPrevButtonClick={onPrevButtonClick}
        onChange={onChange}
        selectedTickets={selectedTickets}
        maxTicketQuantity={6}
      />,
    );
    let element = component.find(`.${styles.ticket} select`).first();

    expect(element.find('option').length).toEqual(6);
  });

  it('should disable next button when selectedTickets is empty', () => {
    let button = component
      .find(`.${styles.buttons}`)
      .find(Button)
      .last();

    expect(button.prop('disabled')).toBeFalsy();

    component.setProps({ selectedTickets: [] });

    button = component
      .find(`.${styles.buttons}`)
      .find(Button)
      .last();

    expect(button.prop('disabled')).toBeTruthy();
  });

  it('should invoke onNextButtonClick when clicking on next button', () => {
    component
      .find(`.${styles.buttons}`)
      .find(Button)
      .last()
      .simulate('click');

    expect(onNextButtonClick.mock.calls.length).toBe(1);
  });

  it('should invoke onPrevButtonClick when clicking on prev button', () => {
    component
      .find(`.${styles.buttons}`)
      .find(Button)
      .first()
      .simulate('click');

    expect(onPrevButtonClick.mock.calls.length).toBe(1);
  });
});
