import React from 'react';
import { shallow } from 'enzyme';
import Loader, { loaderInline } from '../../../src/components/Loader/Loader';

describe('<Loader/>', () => {
  let component;

  it('should render loader', () => {
    component = shallow(<Loader loading={true} />);

    expect(component).toBeDefined();
  });

  it('should not render loader', () => {
    component = shallow(<Loader loading={false} />);

    expect(component.html().length).toEqual(0);
  });

  it('should render inline loader', () => {
    component = shallow(<Loader loading={true} inline={true} />);

    expect(component.prop('className')).toEqual(loaderInline);
  });
});
