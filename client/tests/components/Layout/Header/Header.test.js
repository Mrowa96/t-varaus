import React from 'react';
import { shallow } from 'enzyme';
import Header from '../../../../src/components/Layout/Header/Header';
import { Link } from 'react-router-dom';
import routes from '../../../../src/routes';

describe('<Header/>', () => {
  let header;

  beforeAll(() => {
    header = shallow(<Header />);
  });

  it('renders semantic header', () => {
    expect(header.find('header').length > 0).toBeTruthy();
  });

  it('renders link to home page', () => {
    expect(
      header.find(Link).findWhere(item => {
        return item.prop('to') === routes.home.path;
      }),
    ).toExist();
  });

  it('renders link to facebook', () => {
    expect(
      header.find('a').findWhere(item => {
        return item.prop('href') === '//facebook.com';
      }),
    ).toExist();
  });

  it('renders link to instagram', () => {
    expect(
      header.find('a').findWhere(item => {
        return item.prop('href') === '//instagram.com';
      }),
    ).toExist();
  });
});
