import canDisplayTicketsPage from "../../src/guards/canDisplayTicketsPage";

describe('test canDisplayTicketsPage function', () => {
  it('should return true value', () => {
    expect(canDisplayTicketsPage({
      reservation: {
        eventId: 1,
        showId: 2
      }
    })).toEqual(true);
  });

  it('should return false value because there is no showId', () => {
    expect(canDisplayTicketsPage({
      reservation: {
        showId: null,
        eventId: 1,
      }
    })).toEqual(false);
  });

  it('should return false value because there is no eventId', () => {
    expect(canDisplayTicketsPage({
      reservation: {
        eventId: null,
        showId: 1
      }
    })).toEqual(false);
  });
});
