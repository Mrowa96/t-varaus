import canDisplaySeatsPage from "../../src/guards/canDisplaySeatsPage";

describe('test canDisplaySeatsPage function', () => {
  it('should return true value', () => {
    expect(canDisplaySeatsPage({
      reservation: {
        eventId: 1,
        showId: 1,
        tickets: [
          {}, {}
        ]
      }
    })).toEqual(true);
  });

  it('should return false value - empty tickets array', () => {
    expect(canDisplaySeatsPage({
      reservation: {
        eventId: 1,
        showId: 1,
        tickets: []
      }
    })).toEqual(false);
  });

  it('should return false value - event not selected', () => {
    expect(canDisplaySeatsPage({
      reservation: {
        eventId: null,
        showId: 1,
        tickets: [
          {}, {}
        ]
      }
    })).toEqual(false);
  });

  it('should return false value - show not selected', () => {
    expect(canDisplaySeatsPage({
      reservation: {
        eventId: 1,
        showId: null,
        tickets: [
          {}, {}
        ]
      }
    })).toEqual(false);
  });
});
