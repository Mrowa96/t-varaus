import { compareObjects } from "../../src/utils/compare";

describe('test compareObjects utility', () => {
  it('[compareObjects] should return true', () => {
    const firstObject = {
      id: 1,
      name: 'test',
      deep: {
        test: 'test'
      }
    };
    const secondObject = {
      id: 1,
      name: 'test',
      deep: {
        test: 'test'
      }
    };

    expect(compareObjects(firstObject, secondObject)).toEqual(true);
  });
});
