import { getRouteByReservationStep } from "../../src/utils/routeByReservationStep";

describe('test getRouteByReservationStep utility', () => {
  it('[getRouteByReservationStep] should return correct value', () => {
    const testRoute = {
      path: '/test',
      data: {
        reservationStep: 'test'
      }
    };
    const routes = {
      test: testRoute
    };

    expect(getRouteByReservationStep(routes, 'test')).toEqual(testRoute);
  });

  it('[getRouteByReservationStep] should return undefined value - missing reservation step property from route', () => {
    const testRoute = {
      path: '/test',
      data: {}
    };
    const routes = {
      test: testRoute
    };

    expect(getRouteByReservationStep(routes, 'test')).toEqual(undefined);
  });

  it('[getRouteByReservationStep]should return undefined value - missing data property from route', () => {
    const testRoute = {
      path: '/test',
    };
    const routes = {
      test: testRoute
    };

    expect(getRouteByReservationStep(routes, 'test')).toEqual(undefined);
  });
});
