export default class Seat {
  // TODO Change to css properties
  static defaultColors = {
    free: '#4CAF50',
    selected: '#FF9800',
    taken: '#BDBDBD',
  };

  color = Seat.defaultColors.free;

  constructor(
    x = 0,
    y = 0,
    row = 0,
    column = 0,
    width = 0,
    height = 0,
    status = undefined,
    colors = Seat.defaultColors,
  ) {
    this.x = x;
    this.y = y;
    this.row = row;
    this.column = column;
    this.width = width;
    this.height = height;
    this.colors = colors;

    this.update(status);
  }

  update = status => {
    this.status = status;

    if (typeof this.status === 'undefined') {
      this.color = this.colors.free;
    } else if (this.status === true) {
      this.color = this.colors.selected;
    } else {
      this.color = this.colors.taken;
    }
  };
}
