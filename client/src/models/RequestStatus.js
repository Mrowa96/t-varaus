const RequestStatus = {
  NOT_INITIALIZED: undefined,
  PENDING: 'PENDING',
  SUCCESS: 'SUCCESS',
  FAIL: 'FAIL',
};

export default RequestStatus;
