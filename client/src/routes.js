import HomePage from './pages/HomePage';
import EventDetailPage from './pages/EventDetailPage';
import ReservationPage from './pages/ReservationPage';
import NewsDetailPage from './pages/NewsDetailPage';
import NewsListPage from './pages/NewsListPage';
import AboutUsPage from './pages/AboutUsPage';
import NotFoundPage from './pages/NotFoundPage';
import TicketsPage from './pages/Reservation/TicketsPage';
import canDisplayTicketsPage from './guards/canDisplayTicketsPage';
import SeatsPage from './pages/Reservation/SeatsPage';
import canDisplaySeatsPage from './guards/canDisplaySeatsPage';
import ClientDataPage from './pages/Reservation/ClientDataPage';
import canDisplayClientDataPage from './guards/canDisplayClientDataPage';
import ReservationStep from './models/ReservationStep';
import InitializePage from './pages/Reservation/InitializePage';
import canDisplayInitializePage from './guards/canDisplayInitializePage';
import RestorePage from './pages/Reservation/RestorePage';
import canDisplayRestorePage from './guards/canDisplayRestorePage';

const routes = {
  home: {
    path: '/',
    component: HomePage,
    exact: true,
  },
  eventDetail: {
    path: '/event-detail/:id',
    component: EventDetailPage,
    exact: false,
  },
  reservationClient: {
    path: '/reservation/client',
    component: ClientDataPage,
    exact: false,
    protected: {
      authenticate: canDisplayClientDataPage,
      redirectTo: '/reservation/seats',
    },
    data: {
      reservationStep: ReservationStep.SET_CLIENT_DATA,
    },
  },
  reservationSeats: {
    path: '/reservation/seats',
    component: SeatsPage,
    exact: false,
    protected: {
      authenticate: canDisplaySeatsPage,
      redirectTo: '/reservation/tickets',
    },
    data: {
      reservationStep: ReservationStep.CHOSE_SEATS,
    },
  },
  reservationTickets: {
    path: '/reservation/tickets',
    component: TicketsPage,
    exact: false,
    protected: {
      authenticate: canDisplayTicketsPage,
      redirectTo: '/reservation',
    },
    data: {
      reservationStep: ReservationStep.SELECT_TICKETS,
    },
  },
  reservationRestore: {
    path: '/reservation/restore',
    component: RestorePage,
    exact: true,
    protected: {
      authenticate: canDisplayRestorePage,
      redirectTo: ({ reservation }) =>
        reservation.lastVisitedStep ? reservation.lastVisitedStep : '/reservation/tickets',
    },
    data: {
      reservationStep: ReservationStep.RESTORE,
    },
  },
  reservationInitialize: {
    path: '/reservation/initialize',
    component: InitializePage,
    exact: true,
    protected: {
      authenticate: canDisplayInitializePage,
      redirectTo: '/reservation/tickets',
    },
    data: {
      reservationStep: ReservationStep.INITIALIZE,
    },
  },
  reservation: {
    path: '/reservation',
    component: ReservationPage,
    exact: false,
  },
  newsDetail: {
    path: '/news/detail/:id',
    component: NewsDetailPage,
    exact: false,
  },
  newsList: {
    path: '/news',
    component: NewsListPage,
    exact: false,
  },
  aboutUs: {
    path: '/about-us',
    component: AboutUsPage,
    exact: false,
  },
  notFound: {
    path: undefined,
    component: NotFoundPage,
    exact: false,
  },
};

export default routes;
