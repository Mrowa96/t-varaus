export default function canDisplayTicketsPage({ reservation }) {
  return !!reservation.eventId && !!reservation.showId;
}
