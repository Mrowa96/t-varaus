export default function canDisplayInitializePage({ reservation }) {
  return !!(!reservation.id && (reservation.showId && reservation.startTime));
}
