import canDisplaySeatsPage from './canDisplaySeatsPage';

export default function canDisplayClientDataPage({ reservation }) {
  return !!(canDisplaySeatsPage({ reservation }) && reservation.seats.length);
}
