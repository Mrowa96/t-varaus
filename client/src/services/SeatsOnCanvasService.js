export default class SeatsOnCanvasService {
  static defaultOptions = {
    onSeatClick: () => {},
  };

  constructor(canvas, options = {}) {
    this.canvas = canvas;
    this.context = canvas.getContext('2d');
    this.options = Object.assign(SeatsOnCanvasService.defaultOptions, options);

    this._attachListener();
  }

  draw({ seats, columns, gap }) {
    this.seats = seats;
    this.columns = columns;
    this.gap = gap;

    this.context.clearRect(0, 0, this.canvas.clientWidth, this.canvas.clientHeight);

    this._drawSeats();
    this._drawRowNumbers();
  }

  _drawSeats() {
    this.seats.forEach(seat => {
      this._drawSeat(seat);
    });
  }

  _drawSeat(seat) {
    this.context.fillStyle = seat.color;
    this.context.fillRect(seat.x, seat.y, seat.width, seat.height);

    this.context.font = '18px Source Sans Pro';
    this.context.textAlign = 'center';
    this.context.textBaseline = 'middle';
    this.context.fillStyle = '#000000';
    this.context.fillText(seat.column, seat.x + seat.width / 2, seat.y + seat.height / 2);
  }

  _drawRowNumbers() {
    let x;

    this.seats.forEach(seat => {
      if (seat.column === 1) {
        x = seat.x - this.gap;

        this._drawRowNumber(seat, x);
      } else if (seat.column === this.columns) {
        x = seat.x + seat.width + this.gap;

        this._drawRowNumber(seat, x);
      }
    });
  }

  _drawRowNumber(seat, x) {
    this.context.font = '16px Source Sans Pro';
    this.context.textAlign = 'center';
    this.context.textBaseline = 'middle';
    this.context.fillStyle = '#000000';
    this.context.fillText(seat.row, x, seat.y + seat.height / 2);
  }

  _attachListener() {
    this.canvas.addEventListener('click', event => {
      const x = event.pageX - this.canvas.offsetLeft;
      const y = event.pageY - this.canvas.offsetTop;

      this.seats.forEach(seat => {
        if (y > seat.y && y < seat.y + seat.height && x > seat.x && x < seat.x + seat.width) {
          if (seat.status !== false) {
            this.options.onSeatClick(seat);
          }

          return 0;
        }
      });
    });
  }

  get seats() {
    if (!this._seats.length) {
      throw new Error('Seats array must not be empty.');
    }

    return this._seats;
  }

  set seats(seats) {
    this._seats = seats;
  }
}
