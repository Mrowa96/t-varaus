import Seat from '../models/Seat';

export default class SeatsGeneratorService {
  static defaultOptions = {
    gap: 10,
    maxWidth: 0,
    maxHeight: 0,
  };

  constructor(seatsConfiguration, reservation, options = {}) {
    this.seatsConfiguration = seatsConfiguration;
    this.reservation = reservation;
    this.options = Object.assign(SeatsGeneratorService.defaultOptions, options);
  }

  generateSeats() {
    const seats = [];
    const { gap, maxWidth, maxHeight } = this.options;
    const seatWidth = (maxWidth - 3 * gap) / this.seatsConfiguration.columns - gap;
    const seatHeight = (maxHeight - 3 * gap) / this.seatsConfiguration.rows - gap;
    let row = 1;
    let column;

    while (row <= this.seatsConfiguration.rows) {
      column = 1;

      while (column <= this.seatsConfiguration.columns) {
        const x = (column - 1) * (seatWidth + gap) + 2 * gap;
        const y = (row - 1) * (seatHeight + gap) + 2 * gap;

        const seat = new Seat(x, y, row, column, seatWidth, seatHeight, this._getSeatStatus(row, column));

        seats.push(seat);

        column += 1;
      }

      row += 1;
    }

    return {
      seats,
      columns: this.seatsConfiguration.columns,
      rows: this.seatsConfiguration.rows,
      gap: this.options.gap,
    };
  }

  _getSeatStatus(row, column) {
    let seatStatus;

    if (this.reservation.takenSeats.find(seat => seat.row === row && seat.column === column)) {
      seatStatus = false;
    }

    const selectedSeats = this.reservation.seats;

    if (selectedSeats) {
      const selectedSeat = selectedSeats.find(seat => seat.row === row && seat.column === column);

      if (selectedSeat) {
        seatStatus = true;
      }
    }

    return seatStatus;
  }
}
