import React, { Component } from 'react';
import NewsListContainer from '../containers/NewsListContainer';
import EventSlideshowContainer from '../containers/EventSlideshowContainer';

export default class HomePage extends Component {
  static NEWS_LIMIT = 3;

  static EVENTS_LIMIT = 8;

  static fetchInitialData = () => [
    NewsListContainer.fetchInitialData(HomePage.NEWS_LIMIT),
    EventSlideshowContainer.fetchInitialData(HomePage.EVENTS_LIMIT),
  ];

  render() {
    return (
      <div>
        <NewsListContainer limit={HomePage.NEWS_LIMIT} />
        <EventSlideshowContainer limit={HomePage.EVENTS_LIMIT} />
      </div>
    );
  }
}
