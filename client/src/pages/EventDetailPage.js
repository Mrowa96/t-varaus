import React, { Component } from 'react';
import EventDetailContainer from '../containers/EventDetailContainer';

export default class EventDetailPage extends Component {
  static fetchInitialData = ({ params }) => [EventDetailContainer.fetchInitialData(params)];

  render() {
    return <EventDetailContainer />;
  }
}
