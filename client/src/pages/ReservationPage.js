import React from 'react';
import FilteredEventListContainer from '../containers/FilteredEventList/FilteredEventListContainer';

export default function ReservationPage() {
  return <FilteredEventListContainer />;
}
