import React from 'react';
import ClientDataManagerContainer from '../../containers/Reservation/ClientDataManagerContainer';
import ReservationPageWrapper from '../../components/ReservationPageWrapper/ReservationPageWrapper';

export default function ClientDataPage() {
  return <ReservationPageWrapper WrappedComponent={ClientDataManagerContainer} />;
}
