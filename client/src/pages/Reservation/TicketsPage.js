import React from 'react';
import TicketManagerContainer from '../../containers/Reservation/TicketManagerContainer';
import ReservationPageWrapper from '../../components/ReservationPageWrapper/ReservationPageWrapper';

export default function TicketsPage() {
  return <ReservationPageWrapper WrappedComponent={TicketManagerContainer} />;
}
