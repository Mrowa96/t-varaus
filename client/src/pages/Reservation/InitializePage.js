import React from 'react';
import InitializeContainer from '../../containers/Reservation/InitializeContainer';

export default function InitializePage() {
  return <InitializeContainer />;
}
