import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Swiper from 'react-id-swiper';
import { Link } from 'react-router-dom';
import styles from './EventSlideshow.module.scss';

export default class EventSlideshow extends Component {
  static propTypes = {
    events: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        posterUrl: PropTypes.string.isRequired,
      }),
    ).isRequired,
  };

  constructor(props) {
    super(props);

    this.swiperParams = {
      slidesPerView: 4,
      centeredSlides: false,
      simulateTouch: false,
      shouldSwiperUpdate: true,
      breakpoints: {
        767: {
          slidesPerView: 1,
          spaceBetween: 10,
        },
        1023: {
          slidesPerView: 2,
        },
        1199: {
          slidesPerView: 3,
        },
      },
      on: {
        slideChange: () => {
          this.forceUpdate();
        },
        init: () => {
          this.forceUpdate();
        },
      },
    };
    this.swiper = null;
  }

  onPrevClick = () => {
    if (this.swiper) {
      this.swiper.slidePrev();
    }
  };

  onNextClick = () => {
    if (this.swiper) {
      this.swiper.slideNext();
    }
  };

  _onRef = node => {
    if (node) {
      this.swiper = node.swiper;
    }
  };

  render() {
    const { events } = this.props;
    let content;

    if (events.length) {
      content = (
        <div className={styles.swiperWrapper}>
          <Swiper {...this.swiperParams} ref={this._onRef}>
            {events.map(event => (
              <div className={styles.event} key={`event_${event.id}`}>
                <Link to={`/event-detail/${event.id}`} className={styles.posterWrapper}>
                  <img src={event.posterUrl} alt={event.title} />
                </Link>

                <Link to={`/event-detail/${event.id}`} className={styles.title}>
                  {event.title}
                </Link>
              </div>
            ))}
          </Swiper>

          <button
            type="button"
            className={styles.prevButton}
            onClick={this.onPrevClick}
            aria-disabled={this.swiper ? this.swiper.isBeginning : true}
          >
            <span className={styles.angleLeft} />
          </button>

          <button
            type="button"
            className={styles.nextButton}
            onClick={this.onNextClick}
            aria-disabled={this.swiper ? this.swiper.isEnd : true}
          >
            <span className={styles.angleRight} />
          </button>
        </div>
      );
    } else {
      content = <p>No movies to display</p>;
    }

    return (
      <section className={styles.eventSlideshow}>
        <h1 className={styles.sectionTitle}>Movies</h1>

        {content}
      </section>
    );
  }
}
