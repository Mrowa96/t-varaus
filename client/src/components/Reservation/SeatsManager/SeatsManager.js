import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../../Button/Button';
import styles from './SeatsManager.module.scss';
import SeatsOnCanvasService from '../../../services/SeatsOnCanvasService';
import SeatsGeneratorService from '../../../services/SeatsGeneratorService';

export default class SeatsManager extends Component {
  static propTypes = {
    room: PropTypes.shape({
      seats: PropTypes.shape({
        rows: PropTypes.number.isRequired,
        columns: PropTypes.number.isRequired,
      }).isRequired,
    }).isRequired,
    reservation: PropTypes.shape({
      seats: PropTypes.arrayOf(
        PropTypes.shape({
          row: PropTypes.number.isRequired,
          column: PropTypes.number.isRequired,
        }),
      ).isRequired,
    }).isRequired,
    onPrevButtonClick: PropTypes.func.isRequired,
    onNextButtonClick: PropTypes.func.isRequired,
    onSeatClick: PropTypes.func.isRequired,
    totalTicketsQuantity: PropTypes.number.isRequired,
  };

  get _buttonShouldBeDisabled() {
    const { reservation, totalTicketsQuantity } = this.props;

    return totalTicketsQuantity !== reservation.seats.length;
  }

  constructor(props) {
    super(props);

    this.seatsOnCanvasService = null;
    this.canvas = React.createRef();
  }

  componentDidMount() {
    window.addEventListener('resize', this.drawCanvas);

    this.drawCanvas();
  }

  componentDidUpdate(prevProps) {
    const { reservation } = this.props;

    if (prevProps.reservation.seats.length !== reservation.seats.length) {
      this.drawCanvas();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.drawCanvas);
  }

  drawCanvas = () => {
    const { room, reservation } = this.props;

    if (this.canvas.current) {
      this.canvas.current.width = this.canvas.current.clientWidth;
      this.canvas.current.height = this.canvas.current.clientHeight;

      const seatsGeneratorService = new SeatsGeneratorService(room.seats, reservation, {
        maxWidth: this.canvas.current.clientWidth,
        maxHeight: this.canvas.current.clientHeight,
      });

      if (!this.seatsOnCanvasService) {
        this.seatsOnCanvasService = new SeatsOnCanvasService(this.canvas.current, {
          onSeatClick: this.onChange,
        });
      }

      this.seatsOnCanvasService.draw(seatsGeneratorService.generateSeats());
    }
  };

  onChange = seat => {
    const { onSeatClick } = this.props;

    onSeatClick(seat.column, seat.row);
  };

  render() {
    const { onNextButtonClick, onPrevButtonClick } = this.props;

    return (
      <div className={styles.seatsManager}>
        <h1 className="title">Select seats</h1>

        <div className={styles.seatsChooser}>
          <div className={styles.seatsCanvasWrapper}>
            <canvas className={styles.seatsCanvas} width={500} height={400} ref={this.canvas} />
          </div>
        </div>

        <div className={styles.buttons}>
          <Button type="button" onClick={onPrevButtonClick} text="Prev" />
          <Button type="button" onClick={onNextButtonClick} disabled={this._buttonShouldBeDisabled} />
        </div>
      </div>
    );
  }
}
