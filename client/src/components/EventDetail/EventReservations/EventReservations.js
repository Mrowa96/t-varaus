import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from './EventReservations.module.scss';
import MakeReservationButtonContainer from '../../../containers/MakeReservationButtonContainer';
import { getShowsGroupedByPlace } from '../../../state/selectors/events';

export default class EventReservations extends PureComponent {
  static propTypes = {
    event: PropTypes.shape({
      shows: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.number.isRequired,
          place: PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            room: PropTypes.shape({
              id: PropTypes.number.isRequired,
              name: PropTypes.string.isRequired,
              seats: PropTypes.shape({
                rows: PropTypes.number.isRequired,
                columns: PropTypes.number.isRequired,
              }).isRequired,
            }).isRequired,
          }).isRequired,
          date: PropTypes.string.isRequired,
          startTimes: PropTypes.arrayOf(PropTypes.string).isRequired,
        }),
      ).isRequired,
    }).isRequired,
  };

  get _placesWithShows() {
    const { event } = this.props;

    return getShowsGroupedByPlace(event);
  }

  render() {
    const { event } = this.props;

    return (
      <>
        {!!this._placesWithShows.length && (
          <div className={styles.reservations}>
            <h3 className="subtitle">Make a reservation</h3>

            <div className={styles.placesWrapper}>
              {this._placesWithShows.map(place => (
                <div key={`place_${place.id}`} className={styles.places}>
                  <p className={styles.placeName}>{place.name}</p>

                  <ul className={styles.placeDates}>
                    {place.shows.map(show => (
                      <li key={`show_${show.id}`}>
                        <p className={styles.placeDate}>{show.date}</p>

                        <ul className={styles.placeDateStartTimes}>
                          {show.startTimes.map(startTime => (
                            <li key={`show_${show.id}_start_time_${startTime}`} className={styles.placeDateStartTime}>
                              <MakeReservationButtonContainer
                                eventId={event.id}
                                showId={show.id}
                                place={place}
                                date={show.date}
                                startTime={startTime}
                                text={startTime}
                              />
                            </li>
                          ))}
                        </ul>
                      </li>
                    ))}
                  </ul>
                </div>
              ))}
            </div>
          </div>
        )}
      </>
    );
  }
}
