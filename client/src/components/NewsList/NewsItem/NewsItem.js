import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import styles from './NewsItem.module.scss';

export default class NewsItem extends PureComponent {
  static propTypes = {
    newsItem: PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      category: PropTypes.shape({
        name: PropTypes.string.isRequired,
      }),
      photo: PropTypes.string.isRequired,
    }).isRequired,
  };

  render() {
    const { newsItem } = this.props;

    return (
      <li className={styles.newsItem}>
        {newsItem.category && (
          <div className={styles.categoryWrapper}>
            <span className={styles.category}>{newsItem.category.name}</span>
          </div>
        )}

        <Link to={`/news/detail/${newsItem.id}`} className={styles.photoWrapper}>
          <div className={styles.photo} style={{ backgroundImage: `url('${newsItem.photo}')` }} />
        </Link>
        <Link to={`/news/detail/${newsItem.id}`} className={styles.title}>
          {newsItem.title}
        </Link>
      </li>
    );
  }
}
