import React, { PureComponent } from 'react';
import { css } from 'react-emotion';
import { ClipLoader } from 'react-spinners';
import PropTypes from 'prop-types';

export const loader = css`
  display: block !important;
  margin: 1rem auto;
`;

export const loaderInline = css`
  display: inline-block !important;
`;

export default class Loader extends PureComponent {
  static propTypes = {
    loading: PropTypes.bool.isRequired,
    inline: PropTypes.bool,
  };

  static defaultProps = {
    inline: false,
  };

  render() {
    const { loading, inline } = this.props;

    return (
      <ClipLoader
        className={inline ? loaderInline : loader}
        sizeUnit="px"
        color="var(--c-primary)"
        size={50}
        loading={loading}
      />
    );
  }
}
