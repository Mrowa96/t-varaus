import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './Header.module.scss';
import routes from '../../../routes';

export default function Header() {
  return (
    <header className={styles.header}>
      <Link to={routes.home.path} className={styles.homeLink}>
        <FontAwesomeIcon icon={['fas', 'video']} />
        Cinema
      </Link>

      <div className={styles.socials}>
        <a href="//facebook.com" target="_blank" rel="noopener noreferrer" className={styles.socialLinkFacebook}>
          <span className={styles.label}>Facebook</span>
          <span className={styles.icon}>
            <FontAwesomeIcon icon={['fab', 'facebook-f']} />
          </span>
        </a>

        <a href="//instagram.com" target="_blank" rel="noopener noreferrer" className={styles.socialLinkInstagram}>
          <span className={styles.label}>Instagram</span>
          <span className={styles.icon}>
            <FontAwesomeIcon icon={['fab', 'instagram']} />
          </span>
        </a>
      </div>
    </header>
  );
}
