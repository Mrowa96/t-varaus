import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Swiper from 'react-id-swiper';
import styles from './Navigation.module.scss';
import routes from '../../../routes';

export default class Navigation extends Component {
  constructor(props) {
    super(props);

    this.swiperParams = {
      slidesPerView: 'auto',
      spaceBetween: 10,
      centeredSlides: false,
      freeMode: true,
      simulateTouch: false,
      renderNextButton: () => (
        <button type="button" className={`swiper-button-next ${styles.nextButton}`}>
          <span className={styles.angleRight} />
        </button>
      ),
      navigation: {
        nextEl: '.swiper-button-next',
      },
    };
    this.swiper = null;
  }

  _onRef = node => {
    if (node) {
      this.swiper = node.swiper;
    }
  };

  render() {
    return (
      <nav className={styles.navigation}>
        <Swiper {...this.swiperParams} ref={this._onRef}>
          <NavLink exact to={routes.home.path} className={styles.link} activeClassName={styles.active}>
            Home
          </NavLink>
          <NavLink to={routes.newsList.path} className={styles.link} activeClassName={styles.active}>
            News
          </NavLink>
          <NavLink to={routes.reservation.path} className={styles.link} activeClassName={styles.active}>
            Make a reservation
          </NavLink>
          <NavLink to={routes.aboutUs.path} className={styles.link} activeClassName={styles.active}>
            About us
          </NavLink>
        </Swiper>
      </nav>
    );
  }
}
