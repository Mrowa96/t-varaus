import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './EventItemToReserve.module.scss';
import MakeReservationButtonContainer from '../../../containers/MakeReservationButtonContainer';

export default class EventItemToReserve extends PureComponent {
  static propTypes = {
    event: PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      posterUrl: PropTypes.string.isRequired,
      shows: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.number.isRequired,
          place: PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            room: PropTypes.shape({
              id: PropTypes.number.isRequired,
              name: PropTypes.string.isRequired,
              seats: PropTypes.shape({
                rows: PropTypes.number.isRequired,
                columns: PropTypes.number.isRequired,
              }).isRequired,
            }).isRequired,
          }).isRequired,
          date: PropTypes.string.isRequired,
          startTimes: PropTypes.arrayOf(PropTypes.string).isRequired,
        }),
      ).isRequired,
    }).isRequired,
    selectedPlaceId: PropTypes.number,
    selectedDate: PropTypes.string,
  };

  static defaultProps = {
    selectedPlaceId: undefined,
    selectedDate: undefined,
  };

  get _show() {
    const { event, selectedPlaceId, selectedDate } = this.props;

    if (selectedPlaceId && selectedDate) {
      return event.shows.find(show => show.place.id === selectedPlaceId && show.date === selectedDate);
    }

    return undefined;
  }

  render() {
    const { event } = this.props;

    return (
      <li className={styles.event}>
        <Link to={`/event-detail/${event.id}`} className={styles.eventPhotoWrapper}>
          <img src={event.posterUrl} alt={event.title} />
        </Link>

        <div className={styles.eventNameWrapper}>
          <Link to={`/event-detail/${event.id}`}>{event.title}</Link>
        </div>

        {this._show && (
          <div className={styles.eventStartTimes}>
            {this._show.startTimes.map(startTime => (
              <MakeReservationButtonContainer
                key={`show_${this._show.id}_start_time_${startTime}`}
                eventId={event.id}
                showId={this._show.id}
                place={this._show.place}
                date={this._show.date}
                startTime={startTime}
                text={startTime}
              />
            ))}
          </div>
        )}
      </li>
    );
  }
}
