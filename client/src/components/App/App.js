import React from 'react';
import './App.scss';
import { library, config } from '@fortawesome/fontawesome-svg-core';
import { faFacebookF, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faVideo, faCompass } from '@fortawesome/free-solid-svg-icons';
import Helmet from 'react-helmet';
import routes from '../../routes';
import Layout from '../Layout/Layout';
import RoutesContainer from '../../containers/RoutesContainer';

library.add(faFacebookF, faInstagram, faVideo, faCompass);
config.autoAddCss = false;

export default function App() {
  return (
    <>
      <Helmet>
        <title>Sinehan</title>
        <meta name="description" content="Simple React app to browse movies or news and to reserve tickets" />
      </Helmet>

      <Layout>
        <RoutesContainer routes={routes} />
      </Layout>
    </>
  );
}
