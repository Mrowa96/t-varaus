import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, Switch, withRouter } from 'react-router-dom';
import ProtectedRoute from '../components/ProtectedRoute/ProtectedRoute';

// TODO Test it
class RoutesContainer extends Component {
  static propTypes = {
    routes: PropTypes.shape({
      protected: PropTypes.shape({
        redirectTo: PropTypes.oneOf([PropTypes.string, PropTypes.func]).isRequired,
        authenticate: PropTypes.func.isRequired,
      }),
      path: PropTypes.string,
      exact: PropTypes.bool,
      component: PropTypes.element,
    }).isRequired,
    state: PropTypes.object.isRequired,
  };

  render() {
    const { state } = this.props;

    return (
      <Switch>
        {this._routes.map(([routeName, route]) => {
          if (route.protected) {
            const redirectTo =
              typeof route.protected.redirectTo === 'function'
                ? route.protected.redirectTo(state)
                : route.protected.redirectTo;

            return (
              <ProtectedRoute
                key={`protected_route_${routeName}`}
                path={route.path}
                exact={route.exact}
                component={route.component}
                isAuthenticated={route.protected.authenticate(state)}
                redirectTo={redirectTo}
              />
            );
          }

          return <Route key={`route_${routeName}`} path={route.path} exact={route.exact} component={route.component} />;
        })}
      </Switch>
    );
  }

  get _routes() {
    const { routes } = this.props;

    return Object.entries(routes);
  }
}

const mapStateToProps = state => ({
  state,
});

export default withRouter(
  connect(
    mapStateToProps,
    null,
  )(RoutesContainer),
);
