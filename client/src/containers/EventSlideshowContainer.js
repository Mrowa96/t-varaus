import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { loadEvents as loadEventsAction } from '../state/actions/events';
import EventSlideshow from '../components/EventSlideshow/EventSlideshow';
import Loader from '../components/Loader/Loader';
import { getEventsByFilter } from '../state/selectors/events';

class EventSlideshowContainer extends Component {
  static propTypes = {
    loadEvents: PropTypes.func.isRequired,
    events: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        posterUrl: PropTypes.string.isRequired,
      }),
    ),
    limit: PropTypes.number,
  };

  static fetchInitialData(limit) {
    return loadEventsAction(Symbol('EventSlideshowContainer'), {
      limit,
    });
  }

  static defaultProps = {
    events: undefined,
    limit: 8,
  };

  componentDidMount() {
    if (!this._hasEvents(true)) {
      this._loadEvents();
    }
  }

  render() {
    const { events } = this.props;

    if (!this._hasEvents(true)) {
      return <Loader loading={!this._hasEvents(true)} />;
    }

    return <EventSlideshow events={events} />;
  }

  _loadEvents() {
    const { loadEvents, limit } = this.props;

    loadEvents(Symbol('EventSlideshowContainer'), { limit });
  }

  _hasEvents(strict) {
    const { events } = this.props;

    return strict ? !!events : !!events && !!events.length;
  }
}

const mapStateToProps = (state, props) => ({
  events: getEventsByFilter(state.events, { limit: props.limit }),
});

const mapDispatchToProps = dispatch => ({
  loadEvents: (requestId, filters) => {
    dispatch(loadEventsAction(requestId, filters));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EventSlideshowContainer);
