import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loadNews as loadNewsAction } from '../state/actions/news';
import NewsList from '../components/NewsList/NewsList';
import Loader from '../components/Loader/Loader';
import { getNewsByFilter } from '../state/selectors/news';

class NewsListContainer extends Component {
  static propTypes = {
    loadNews: PropTypes.func.isRequired,
    news: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        photo: PropTypes.string,
        category: PropTypes.shape({
          name: PropTypes.string,
        }),
        content: PropTypes.string,
        author: PropTypes.shape({
          name: PropTypes.string,
        }),
      }),
    ),
    limit: PropTypes.number,
  };

  static defaultProps = {
    news: undefined,
    limit: 3,
  };

  static fetchInitialData(limit) {
    return loadNewsAction(Symbol('NewsListContainer'), {
      limit,
    });
  }

  componentDidMount() {
    if (!this._hasNews(true)) {
      this._loadNews();
    }
  }

  render() {
    const { news } = this.props;

    if (!this._hasNews(true)) {
      return <Loader loading={!this._hasNews(true)} />;
    }

    return <NewsList news={news} linkableTitle showSeeAllButton />;
  }

  _hasNews(strict = false) {
    const { news } = this.props;

    return strict ? !!news : !!news && !!news.length;
  }

  _loadNews() {
    const { limit, loadNews } = this.props;

    loadNews(Symbol('NewsListContainer'), { limit });
  }
}

const mapStateToProps = (state, props) => ({
  news: getNewsByFilter(state.news, { limit: props.limit }),
});

const mapDispatchToProps = dispatch => ({
  loadNews: (requestId, filters) => {
    dispatch(loadNewsAction(requestId, filters));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewsListContainer);
