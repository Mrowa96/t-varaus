import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import SeatsManager from '../../components/Reservation/SeatsManager/SeatsManager';
import { getRoomFromReservation, getTotalTicketsQuantity } from '../../state/selectors/reservation';
import {
  addOrRemoveSeat as addOrRemoveSeatAction,
  saveReservation as saveReservationAction,
  setReservationStep as setReservationStepAction,
} from '../../state/actions/reservation';
import ReservationStep from '../../models/ReservationStep';
import routes from '../../routes';

class SeatsManagerContainer extends Component {
  static propTypes = {
    room: PropTypes.object.isRequired,
    reservation: PropTypes.object.isRequired,
    totalTicketsQuantity: PropTypes.number.isRequired,
    addOrRemoveSeat: PropTypes.func.isRequired,
    setReservationStep: PropTypes.func.isRequired,
    saveReservation: PropTypes.func.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentDidMount() {
    const { setReservationStep } = this.props;

    setReservationStep(ReservationStep.CHOSE_SEATS);
  }

  componentDidUpdate(prevProps) {
    const { saveReservation, reservation } = this.props;

    if (prevProps.reservation.seats.length !== reservation.seats.length) {
      saveReservation(Symbol('SeatsManagerContainer'));
    }
  }

  onSeatClick = (column, row) => {
    const { addOrRemoveSeat } = this.props;

    addOrRemoveSeat(column, row);
  };

  onNextButtonClick = () => {
    const { history } = this.props;

    history.push(routes.reservationClient.path);
  };

  onPrevButtonClick = () => {
    const { history } = this.props;

    history.push(routes.reservationTickets.path);
  };

  render() {
    const { room, reservation, totalTicketsQuantity } = this.props;

    return (
      <SeatsManager
        room={room}
        reservation={reservation}
        totalTicketsQuantity={totalTicketsQuantity}
        onNextButtonClick={this.onNextButtonClick}
        onPrevButtonClick={this.onPrevButtonClick}
        onSeatClick={this.onSeatClick}
      />
    );
  }
}

const mapStateToProps = state => ({
  room: getRoomFromReservation(state),
  reservation: state.reservation,
  totalTicketsQuantity: getTotalTicketsQuantity(state),
});

const mapDispatchToProps = dispatch => ({
  addOrRemoveSeat: (column, row) => {
    dispatch(addOrRemoveSeatAction(column, row));
  },
  setReservationStep: step => {
    dispatch(setReservationStepAction(step));
  },
  saveReservation: requestId => {
    dispatch(saveReservationAction(['seats'], requestId));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(SeatsManagerContainer),
);
