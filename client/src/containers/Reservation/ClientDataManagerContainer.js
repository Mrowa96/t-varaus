import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  clearReservation as clearReservationAction,
  completeReservation as completeReservationAction,
  saveReservation as saveReservationAction,
  setReservationStep as setReservationStepAction,
  updateAgreement as updateAgreementAction,
  updateClientData as updateClientDataAction,
} from '../../state/actions/reservation';
import ClientDataManager from '../../components/Reservation/ClientDataManager/ClientDataManager';
import ReservationStep from '../../models/ReservationStep';
import routes from '../../routes';

class ClientDataManagerContainer extends Component {
  static propTypes = {
    reservation: PropTypes.object.isRequired,
    updateClientData: PropTypes.func.isRequired,
    updateAgreement: PropTypes.func.isRequired,
    saveAgreements: PropTypes.func.isRequired,
    saveClientData: PropTypes.func.isRequired,
    clearReservation: PropTypes.func.isRequired,
    completeReservation: PropTypes.func.isRequired,
    setReservationStep: PropTypes.func.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentDidMount() {
    const { setReservationStep } = this.props;

    setReservationStep(ReservationStep.SET_CLIENT_DATA);
  }

  onClientDataChange = (property, value) => {
    const { updateClientData } = this.props;

    updateClientData(property, value);
  };

  onAgreementChange = (agreement, value) => {
    const { updateAgreement, saveAgreements } = this.props;

    updateAgreement(agreement, value);
    saveAgreements(Symbol('ClientDataManagerContainer'));
  };

  onTriggerClientDataSave = () => {
    const { saveClientData } = this.props;

    saveClientData(Symbol('ClientDataManagerContainer'));
  };

  onNextButtonClick = () => {
    const { clearReservation, completeReservation, history, reservation } = this.props;

    history.push(routes.home.path);

    // TODO Display some notification
    completeReservation(reservation.id, Symbol('ClientDataManagerContainer'));
    clearReservation();
  };

  onPrevButtonClick = () => {
    const { history } = this.props;

    history.push(routes.reservationSeats.path);
  };

  render() {
    const { reservation } = this.props;

    return (
      <ClientDataManager
        reservation={reservation}
        onClientDataChange={this.onClientDataChange}
        onAgreementChange={this.onAgreementChange}
        triggerClientDataSave={this.onTriggerClientDataSave}
        onNextButtonClick={this.onNextButtonClick}
        onPrevButtonClick={this.onPrevButtonClick}
      />
    );
  }
}

const mapStateToProps = ({ reservation }) => ({ reservation });

const mapDispatchToProps = dispatch => ({
  updateClientData: (property, value) => {
    dispatch(updateClientDataAction(property, value));
  },
  clearReservation: () => {
    dispatch(clearReservationAction());
  },
  setReservationStep: step => {
    dispatch(setReservationStepAction(step));
  },
  updateAgreement: (agreement, value) => {
    dispatch(updateAgreementAction(agreement, value));
  },
  saveClientData: requestId => {
    dispatch(saveReservationAction(['clientData'], requestId));
  },
  saveAgreements: requestId => {
    dispatch(saveReservationAction(['agreements'], requestId));
  },
  completeReservation: (reservationId, requestId) => {
    dispatch(completeReservationAction(reservationId, requestId));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ClientDataManagerContainer),
);
