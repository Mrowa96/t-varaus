import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import QuickSummary from '../../components/Reservation/QuickSummary/QuickSummary';
import { getEventFromReservation } from '../../state/selectors/events';
import {
  getGroupedSeatsFromReservation,
  getPlaceFromReservation,
  getTotalCost,
} from '../../state/selectors/reservation';

class QuickSummaryContainer extends PureComponent {
  static propTypes = {
    event: PropTypes.shape({
      title: PropTypes.string.isRequired,
      posterUrl: PropTypes.string.isRequired,
      duration: PropTypes.number,
    }).isRequired,
    place: PropTypes.shape({
      name: PropTypes.string.isRequired,
    }).isRequired,
    tickets: PropTypes.objectOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
      }),
    ),
    reservation: PropTypes.shape({
      date: PropTypes.string.isRequired,
      startTime: PropTypes.string.isRequired,
      tickets: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.number.isRequired,
          quantity: PropTypes.number.isRequired,
        }),
      ).isRequired,
      seats: PropTypes.arrayOf(
        PropTypes.shape({
          row: PropTypes.number.isRequired,
          column: PropTypes.number.isRequired,
        }),
      ).isRequired,
    }).isRequired,
    seats: PropTypes.arrayOf(
      PropTypes.shape({
        row: PropTypes.number.isRequired,
        columns: PropTypes.arrayOf(PropTypes.number).isRequired,
      }),
    ).isRequired,
    totalCost: PropTypes.number.isRequired,
  };

  static defaultProps = {
    tickets: undefined,
  };

  render() {
    const { event, reservation, place, tickets, totalCost, seats } = this.props;

    return (
      <QuickSummary
        event={event}
        reservation={reservation}
        place={place}
        seats={seats}
        tickets={tickets}
        totalCost={totalCost}
      />
    );
  }
}

const mapStateToProps = state => ({
  event: getEventFromReservation(state),
  place: getPlaceFromReservation(state),
  seats: getGroupedSeatsFromReservation(state),
  tickets: state.tickets.byId,
  reservation: state.reservation,
  totalCost: getTotalCost(state),
});

export default withRouter(connect(mapStateToProps)(QuickSummaryContainer));
