import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getRequestById } from '../../state/selectors/requests';
import RequestStatus from '../../models/RequestStatus';
import { loadEvent as loadEventAction } from '../../state/actions/events';
import { loadTickets as loadTicketsAction } from '../../state/actions/tickets';
import { apiGet } from '../../utils/request';
import ReservationStatus from '../../models/ReservationStatus';
import Restore from '../../components/Reservation/Restore/Restore';
import { clearReservation as clearReservationAction } from '../../state/actions/reservation';

// TODO Test it
class RestoreContainer extends Component {
  static propTypes = {
    reservation: PropTypes.shape({
      id: PropTypes.string.isRequired,
      eventId: PropTypes.number.isRequired,
    }).isRequired,
    requests: PropTypes.shape({
      status: PropTypes.string,
    }).isRequired,
    loadEvent: PropTypes.func.isRequired,
    loadTickets: PropTypes.func.isRequired,
    clearReservation: PropTypes.func.isRequired,
    displayTime: PropTypes.number,
    history: PropTypes.shape({
      goBack: PropTypes.func.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    displayTime: 2000,
  };

  #loadEventRequestId = Symbol('RestoreContainer');

  #loadTicketsRequestId = Symbol('RestoreContainer');

  componentDidMount() {
    const { reservation, loadEvent, loadTickets, displayTime } = this.props;

    // TODO Add notification
    setTimeout(async () => {
      try {
        const { data: apiReservation } = await apiGet(`reservation/${reservation.id}`);

        if (apiReservation.status === ReservationStatus.IN_PROGRESS) {
          loadEvent(reservation.eventId, this.#loadEventRequestId);
          loadTickets(this.#loadTicketsRequestId);
        } else {
          this._displayMessageAndRedirect('Reservation is completed.', true);
        }
      } catch (e) {
        this._displayMessageAndRedirect("Reservation can't be validated. Try again later.");
      }
    }, displayTime);
  }

  componentDidUpdate() {
    // TODO Add notification
    if (this.loadEventRequest && this.loadEventRequest.status === RequestStatus.FAIL) {
      this._displayMessageAndRedirect("Event can't be loaded. Try again later.");
    }

    if (this.loadTicketsRequest && this.loadTicketsRequest.status === RequestStatus.FAIL) {
      this._displayMessageAndRedirect("Tickets can't be loaded. Try again later.");
    }
  }

  render() {
    return <Restore />;
  }

  _displayMessageAndRedirect = (message, clear = false) => {
    const { clearReservation, history } = this.props;

    console.error(message);

    if (clear) {
      clearReservation();
    }

    history.goBack();
  };

  get loadEventRequest() {
    const { requests } = this.props;

    return getRequestById(requests, this.#loadEventRequestId);
  }

  get loadTicketsRequest() {
    const { requests } = this.props;

    return getRequestById(requests, this.#loadTicketsRequestId);
  }
}

const mapStateToProps = ({ reservation, requests }) => ({
  reservation,
  requests,
});

const mapDispatchToProps = dispatch => ({
  loadEvent: (id, requestId) => {
    dispatch(loadEventAction(id, requestId));
  },
  loadTickets: requestId => {
    dispatch(loadTicketsAction(requestId));
  },
  clearReservation: () => {
    dispatch(clearReservationAction());
  },
});

export const RestoreConnected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(RestoreContainer);

export default withRouter(RestoreConnected);
