import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import TicketManager from '../../components/Reservation/TicketManager/TicketManager';
import { loadTickets as loadTicketsAction } from '../../state/actions/tickets';
import {
  changeTicketQuantity as changeTicketQuantityAction,
  saveReservation as saveReservationAction,
  setReservationStep as setReservationStepAction,
} from '../../state/actions/reservation';
import Loader from '../../components/Loader/Loader';
import ReservationStep from '../../models/ReservationStep';
import routes from '../../routes';
import { getTicketsFromState, hasTickets as hasTicketsSelector } from '../../state/selectors/tickets';

// TODO Test it
class TicketManagerContainer extends Component {
  static propTypes = {
    tickets: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
      }),
    ),
    hasTickets: PropTypes.bool.isRequired,
    loadTickets: PropTypes.func.isRequired,
    changeTicketQuantity: PropTypes.func.isRequired,
    selectedTickets: PropTypes.array.isRequired,
    setReservationStep: PropTypes.func.isRequired,
    saveReservation: PropTypes.func.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    tickets: [],
  };

  componentDidMount() {
    const { setReservationStep, loadTickets } = this.props;

    setReservationStep(ReservationStep.SELECT_TICKETS);
    loadTickets();
  }

  onTicketQuantityChange = (ticketId, quantity) => {
    const { changeTicketQuantity, saveReservation } = this.props;

    changeTicketQuantity(ticketId, quantity);
    saveReservation();
  };

  onNextButtonClick = () => {
    const { history } = this.props;

    history.push(routes.reservationSeats.path);
  };

  onPrevButtonClick = () => {
    const { history } = this.props;

    // TODO? Save in state from which page user make reservation
    history.push(routes.reservation.path);
  };

  render() {
    const { tickets, selectedTickets, hasTickets } = this.props;

    if (!hasTickets) {
      return <Loader loading={!hasTickets} />;
    }

    return (
      <TicketManager
        tickets={tickets}
        onChange={this.onTicketQuantityChange}
        selectedTickets={selectedTickets}
        onPrevButtonClick={this.onPrevButtonClick}
        onNextButtonClick={this.onNextButtonClick}
      />
    );
  }
}

const mapStateToProps = state => ({
  tickets: getTicketsFromState(state),
  hasTickets: hasTicketsSelector(state),
  selectedTickets: state.reservation.tickets,
});

const mapDispatchToProps = dispatch => ({
  loadTickets: () => {
    dispatch(loadTicketsAction());
  },
  changeTicketQuantity: (ticketId, quantity) => {
    dispatch(changeTicketQuantityAction(ticketId, quantity));
  },
  saveReservation: () => {
    dispatch(saveReservationAction(['tickets']));
  },
  setReservationStep: step => {
    dispatch(setReservationStepAction(step));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(TicketManagerContainer),
);
