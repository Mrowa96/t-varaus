import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { loadEvent as loadEventAction } from '../state/actions/events';
import EventDetail from '../components/EventDetail/EventDetail';
import { getEventById } from '../state/selectors/events';
import Loader from '../components/Loader/Loader';

class EventDetailContainer extends Component {
  static propTypes = {
    loadEvent: PropTypes.func.isRequired,
    event: PropTypes.oneOfType([
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        trailerUrl: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        shows: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number.isRequired,
            place: PropTypes.shape({
              id: PropTypes.number.isRequired,
              name: PropTypes.string.isRequired,
              room: PropTypes.shape({
                id: PropTypes.number.isRequired,
                name: PropTypes.string.isRequired,
                seats: PropTypes.shape({
                  rows: PropTypes.number.isRequired,
                  columns: PropTypes.number.isRequired,
                }).isRequired,
              }).isRequired,
            }).isRequired,
            date: PropTypes.string.isRequired,
            startTimes: PropTypes.arrayOf(PropTypes.string).isRequired,
          }),
        ).isRequired,
      }),
      PropTypes.exact({}),
    ]),
    updateMeta: PropTypes.bool,
    match: PropTypes.shape({
      params: PropTypes.object.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    event: undefined,
    updateMeta: true,
  };

  static fetchInitialData = ({ id }) => loadEventAction(id, Symbol('EventDetailContainer'));

  componentDidMount() {
    const { loadEvent } = this.props;

    if (!this._hasEvent()) {
      loadEvent(this._id, Symbol('EventDetailContainer'));
    }
  }

  render() {
    const { event, updateMeta } = this.props;

    if (!this._hasEvent()) {
      return <Loader loading={!this._hasEvent()} />;
    }

    return <EventDetail event={event} updateMeta={updateMeta} />;
  }

  _hasEvent() {
    const { event } = this.props;

    return !!event;
  }

  get _id() {
    const { match } = this.props;

    return match.params.id;
  }
}

const mapStateToProps = (state, { match }) => ({
  event: getEventById(state.events, match.params.id),
});

const mapDispatchToProps = dispatch => ({
  loadEvent: (id, requestId) => {
    dispatch(loadEventAction(id, requestId));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(EventDetailContainer),
);
