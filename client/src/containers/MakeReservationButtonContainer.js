import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  deleteReservation as deleteReservationAction,
  setReservationParameters as setReservationParametersAction,
} from '../state/actions/reservation';
import Button from '../components/Button/Button';
import routes from '../routes';

class MakeReservationButtonContainer extends Component {
  static propTypes = {
    showId: PropTypes.number.isRequired,
    startTime: PropTypes.string.isRequired,
    setReservationParameters: PropTypes.func.isRequired,
    deleteReservation: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
    reservationId: PropTypes.string,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    reservationId: undefined,
  };

  onClick = () => {
    const { history, showId, startTime, setReservationParameters, deleteReservation, reservationId } = this.props;

    if (reservationId) {
      deleteReservation(reservationId, Symbol('MakeReservationButtonContainer'));
    }

    setReservationParameters({ showId, startTime });

    history.push(routes.reservationInitialize.path);
  };

  render() {
    const { text } = this.props;

    return <Button type="button" text={text} onClick={this.onClick} />;
  }
}

const mapStateToProps = ({ reservation }) => ({
  reservationId: reservation.id,
});

const mapDispatchToProps = dispatch => ({
  setReservationParameters: parameters => {
    dispatch(setReservationParametersAction(parameters));
  },
  deleteReservation: (reservationId, requestId) => {
    dispatch(deleteReservationAction(reservationId, requestId));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(MakeReservationButtonContainer),
);
