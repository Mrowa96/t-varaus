import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import EventPlaceFilter from '../../components/FilteredEventList/EventPlaceFilter/EventPlaceFilter';
import { loadPlaces as loadPlacesAction } from '../../state/actions/places';
import { setEventsPlaceIdFilter } from '../../state/actions/events';
import { getPlacesFromState } from '../../state/selectors/places';

class EventPlaceFilterContainer extends Component {
  static propTypes = {
    places: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    }),
    loadPlaces: PropTypes.func.isRequired,
    setFilter: PropTypes.func.isRequired,
    selectedPlaceId: PropTypes.number,
    locked: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    places: undefined,
    selectedPlaceId: undefined,
  };

  componentDidMount() {
    const { loadPlaces } = this.props;

    if (!this._hasPlaces(true)) {
      loadPlaces(Symbol('EventPlaceFilterContainer'));
    }
  }

  onPlaceChange = placeId => {
    const { setFilter } = this.props;

    setFilter(placeId);
  };

  render() {
    const { places, selectedPlaceId, locked } = this.props;

    return (
      <>
        {this._hasPlaces(true) && (
          <EventPlaceFilter
            places={places}
            onChange={this.onPlaceChange}
            selectedPlaceId={selectedPlaceId}
            locked={locked}
          />
        )}
      </>
    );
  }

  _hasPlaces(strict = false) {
    const { places } = this.props;

    return strict ? !!places : !!places && !!Object.keys(places).length;
  }
}

const mapStateToProps = state => ({
  places: getPlacesFromState(state),
  selectedPlaceId: state.events.filters.placeId,
});

const mapDispatchToProps = dispatch => ({
  loadPlaces: requestId => {
    dispatch(loadPlacesAction(requestId));
  },
  setFilter: placeId => {
    dispatch(setEventsPlaceIdFilter(placeId));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EventPlaceFilterContainer);
