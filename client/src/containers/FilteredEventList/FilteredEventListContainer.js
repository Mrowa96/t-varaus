import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import moment from 'moment';
import {
  loadEvents as loadEventsAction,
  setEventsDateFilter as setEventsDateFilterAction,
} from '../../state/actions/events';
import { compareObjects } from '../../utils/compare';
import { getEventsByFilterForDate, getNotEmptyFilters } from '../../state/selectors/events';
import FilteredEventList from '../../components/FilteredEventList/FilteredEventList';

export class FilteredEventListContainer extends Component {
  static propTypes = {
    loadEvents: PropTypes.func.isRequired,
    setEventsDateFilter: PropTypes.func.isRequired,
    events: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        posterUrl: PropTypes.string.isRequired,
        shows: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number.isRequired,
            place: PropTypes.shape({
              id: PropTypes.number.isRequired,
              name: PropTypes.string.isRequired,
              room: PropTypes.shape({
                id: PropTypes.number.isRequired,
                name: PropTypes.string.isRequired,
                seats: PropTypes.shape({
                  rows: PropTypes.number.isRequired,
                  columns: PropTypes.number.isRequired,
                }).isRequired,
              }).isRequired,
            }).isRequired,
            date: PropTypes.string.isRequired,
            startTimes: PropTypes.arrayOf(PropTypes.string).isRequired,
          }),
        ).isRequired,
      }),
    ),
    filters: PropTypes.shape({
      placeId: PropTypes.number,
      date: PropTypes.string,
    }),
  };

  static defaultProps = {
    events: undefined,
    filters: undefined,
  };

  componentDidMount() {
    const { filters, setEventsDateFilter } = this.props;

    if (!filters || !filters.date) {
      setEventsDateFilter(moment().format('YYYY-MM-DD'));
    }

    if (this._isLoading) {
      this._loadEvents();
    }
  }

  componentDidUpdate(prevProps) {
    const { filters } = this.props;

    if (this._hasBothFilters && compareObjects(prevProps.filters, filters) === false) {
      this._loadEvents();
    }
  }

  render() {
    const { events, filters } = this.props;

    return <FilteredEventList eventsLoading={this._isLoading} events={events} filters={filters} />;
  }

  _loadEvents() {
    const { loadEvents, filters } = this.props;

    loadEvents(Symbol('FilteredEventListContainer'), filters);
  }

  _hasEvents(strict = false) {
    const { events } = this.props;

    return strict ? !!events : !!events && !!events.length;
  }

  get _isLoading() {
    return !!this._hasBothFilters && !this._hasEvents(true);
  }

  get _hasBothFilters() {
    const { filters } = this.props;

    return filters && filters.placeId && filters.date;
  }
}

const mapStateToProps = state => {
  let dateTime = moment(state.events.filters.date).format('YYYY-MM-DD HH:mm');

  if (moment().format('YYYY-MM-DD') === state.events.filters.date) {
    dateTime = moment().format('YYYY-MM-DD HH:mm');
  }

  return {
    events: getEventsByFilterForDate(state.events, state.events.filters, dateTime),
    filters: getNotEmptyFilters(state),
  };
};

const mapDispatchToProps = dispatch => ({
  loadEvents: (requestId, filters) => {
    dispatch(loadEventsAction(requestId, filters));
  },
  setEventsDateFilter: date => {
    dispatch(setEventsDateFilterAction(date));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(FilteredEventListContainer),
);
