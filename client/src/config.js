export default {
  apiUrl: process.env.APP_API_URL,
  stateInStorageKey: process.env.APP_STATE_IN_STORAGE_KEY,
};
