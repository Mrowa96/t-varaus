import { put, select } from 'redux-saga/effects';
import { requestPost, requestPut, requestDelete } from '../actions/requests';
import { reservationCreated } from '../actions/reservation';

export function* createReservation(action) {
  const { showId, startTime, requestId } = action.payload;

  yield put(requestPost('reservation', { showId, time: startTime }, requestId, reservationCreated));
}

export function* saveReservation(action) {
  const state = yield select();
  const { properties, requestId } = action.payload;
  const data = {};

  properties.forEach(name => {
    if (Object.prototype.hasOwnProperty.call(state.reservation, name)) {
      data[name] = state.reservation[name];
    }
  });

  if (properties.includes('agreements') && state.reservation.agreements.dataProcessing) {
    data.clientData = state.reservation.clientData;
  }

  yield put(requestPut(`reservation/${state.reservation.id}`, data, requestId));
}

export function* deleteReservation(action) {
  const { reservationId, requestId } = action.payload;

  yield put(requestDelete(`reservation/${reservationId}`, {}, requestId));
}

export function* completeReservation(action) {
  const { reservationId, requestId } = action.payload;

  yield put(requestPut(`reservation/${reservationId}`, { complete: true }, requestId));
}
