import { call, put, select } from 'redux-saga/effects';
import { apiGet, apiPost, apiPut, apiDelete } from '../../utils/request';
import {
  addRequest,
  updateRequest,
  requestFailed as requestFailedAction,
  requestSucceed as requestSucceedAction,
} from '../actions/requests';
import RequestStatus from '../../models/RequestStatus';
import RequestMethod from '../../models/RequestMethod';
import { getRequestById } from '../selectors/requests';

export function* requestGet(action) {
  const { endpoint, filters, requestId, succeedAction, failedAction } = action.payload;

  try {
    yield put(addRequest(requestId, RequestMethod.GET, endpoint, filters));

    const response = yield call(apiGet, endpoint, filters);

    yield put(requestSucceedAction(response, requestId, succeedAction));
  } catch (error) {
    yield put(requestFailedAction(requestId, failedAction));
  }
}

export function* requestDelete(action) {
  const { endpoint, filters, requestId, succeedAction, failedAction } = action.payload;

  try {
    yield put(addRequest(requestId, RequestMethod.DELETE, endpoint, filters));

    const response = yield call(apiDelete, endpoint, filters);

    yield put(requestSucceedAction(response, requestId, succeedAction));
  } catch (error) {
    yield put(requestFailedAction(requestId, failedAction));
  }
}

export function* requestPost(action) {
  const { endpoint, data, requestId, succeedAction, failedAction } = action.payload;

  try {
    yield put(addRequest(requestId, RequestMethod.POST, endpoint, {}, data));

    const response = yield call(apiPost, endpoint, data);

    yield put(requestSucceedAction(response, requestId, succeedAction));
  } catch (error) {
    yield put(requestFailedAction(requestId, failedAction));
  }
}

export function* requestPut(action) {
  const { endpoint, data, requestId, succeedAction, failedAction } = action.payload;

  try {
    yield put(addRequest(requestId, RequestMethod.PUT, endpoint, {}, data));

    const response = yield call(apiPut, endpoint, data);

    yield put(requestSucceedAction(response, requestId, succeedAction));
  } catch (error) {
    yield put(requestFailedAction(requestId, failedAction));
  }
}

export function* requestSucceed(action) {
  const { response, requestId, actionToDispatch } = action.payload;
  const state = yield select();

  if (actionToDispatch) {
    yield put(actionToDispatch(response, getRequestById(state.requests, requestId)));
  }

  yield put(updateRequest(requestId, { status: RequestStatus.SUCCESS }));
}

export function* requestFailed(action) {
  const { requestId, actionToDispatch } = action.payload;
  const state = yield select();

  if (actionToDispatch) {
    yield put(actionToDispatch(getRequestById(state.requests, requestId)));
  }

  yield put(updateRequest(requestId, { status: RequestStatus.FAIL }));
}
