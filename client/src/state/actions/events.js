export const FILTERED_EVENTS_FETCHED = 'FILTERED_EVENTS_FETCHED';
export const EVENT_FETCHED = 'EVENT_FETCHED';
export const EVENT_NOT_FOUND = 'EVENT_NOT_FOUND';
export const LOAD_EVENT = 'LOAD_EVENT';
export const LOAD_EVENTS = 'LOAD_EVENTS';
export const SET_EVENTS_PLACE_ID_FILTER = 'SET_EVENTS_PLACE_ID_FILTER';
export const SET_EVENTS_DATE_FILTER = 'SET_EVENTS_DATE_FILTER';

export function filteredEventsFetched(response, request) {
  return {
    type: FILTERED_EVENTS_FETCHED,
    payload: {
      response,
      request,
    },
  };
}

export function eventFetched(response, request) {
  return {
    type: EVENT_FETCHED,
    payload: {
      response,
      request,
    },
  };
}

export function eventNotFound(request) {
  return {
    type: EVENT_NOT_FOUND,
    payload: {
      request,
    },
  };
}

export function loadEvent(id, requestId) {
  return {
    type: LOAD_EVENT,
    payload: {
      id,
      requestId,
    },
  };
}

export function loadEvents(requestId, filters) {
  return {
    type: LOAD_EVENTS,
    payload: {
      requestId,
      filters,
    },
  };
}

export function setEventsPlaceIdFilter(placeId) {
  return {
    type: SET_EVENTS_PLACE_ID_FILTER,
    payload: {
      placeId,
    },
  };
}

export function setEventsDateFilter(date) {
  return {
    type: SET_EVENTS_DATE_FILTER,
    payload: {
      date,
    },
  };
}
