export const LOAD_TICKETS = 'LOAD_TICKETS';
export const TICKETS_FETCHED = 'TICKETS_FETCHED';

export function ticketsFetched(response) {
  return {
    type: TICKETS_FETCHED,
    payload: {
      response,
    },
  };
}

export function loadTickets(requestId = Symbol('loadTickets'), forceOverride = false) {
  return {
    type: LOAD_TICKETS,
    payload: {
      requestId,
      forceOverride,
    },
  };
}
