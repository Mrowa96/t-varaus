export const REQUEST_GET = 'REQUEST_GET';
export const REQUEST_DELETE = 'REQUEST_DELETE';
export const REQUEST_POST = 'REQUEST_POST';
export const REQUEST_PUT = 'REQUEST_PUT';
export const REQUEST_SUCCEED = 'REQUEST_SUCCEED';
export const REQUEST_FAILED = 'REQUEST_FAILED';
export const ADD_REQUEST = 'ADD_REQUEST';
export const UPDATE_REQUEST = 'UPDATE_REQUEST';

export function requestGet(endpoint, filters, requestId, succeedAction, failedAction) {
  return {
    type: REQUEST_GET,
    payload: {
      requestId,
      endpoint,
      filters,
      succeedAction,
      failedAction,
    },
  };
}

export function requestDelete(endpoint, filters, requestId, succeedAction, failedAction) {
  return {
    type: REQUEST_DELETE,
    payload: {
      requestId,
      endpoint,
      filters,
      succeedAction,
      failedAction,
    },
  };
}

export function requestPost(endpoint, data, requestId, succeedAction, failedAction) {
  return {
    type: REQUEST_POST,
    payload: {
      requestId,
      endpoint,
      data,
      succeedAction,
      failedAction,
    },
  };
}

export function requestPut(endpoint, data, requestId, succeedAction, failedAction) {
  return {
    type: REQUEST_PUT,
    payload: {
      requestId,
      endpoint,
      data,
      succeedAction,
      failedAction,
    },
  };
}

export function requestSucceed(response, requestId, actionToDispatch) {
  return {
    type: REQUEST_SUCCEED,
    payload: {
      response,
      requestId,
      actionToDispatch,
    },
  };
}

export function requestFailed(requestId, actionToDispatch) {
  return {
    type: REQUEST_FAILED,
    payload: {
      requestId,
      actionToDispatch,
    },
  };
}

export function addRequest(requestId, method, endpoint, filters, data) {
  return {
    type: ADD_REQUEST,
    payload: {
      requestId,
      method,
      endpoint,
      filters,
      data,
    },
  };
}

export function updateRequest(requestId, data) {
  return {
    type: UPDATE_REQUEST,
    payload: {
      requestId,
      data,
    },
  };
}
