export function getTicketsFromState(state) {
  if (state.tickets.byId) {
    return Object.values(state.tickets.byId);
  }
}

export function hasTickets(state) {
  return !!getTicketsFromState(state);
}
