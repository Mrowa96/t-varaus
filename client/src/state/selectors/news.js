import { serializeFilters } from '../../utils/serializeFilters';

export function getOneNewsById(newsFromState, id) {
  return newsFromState.byId[id];
}

export function getNewsByFilter(newsFromState, filter) {
  const dataByFilter = newsFromState.dataByFilter[serializeFilters(filter)];
  let newsCollection;

  if (dataByFilter) {
    newsCollection = [];

    dataByFilter.ids.reduce((news, id) => {
      const oneNews = getOneNewsById(newsFromState, id);

      if (oneNews) {
        news.push(oneNews);
      }

      return news;
    }, newsCollection);
  }

  return newsCollection;
}

export function getNewsTotalQuantityByFilter(newsFromState, filter) {
  const dataByFilter = newsFromState.dataByFilter[serializeFilters(filter)];

  if (dataByFilter && typeof dataByFilter.totalQuantity !== 'undefined') {
    return dataByFilter.totalQuantity;
  }

  return 0;
}
