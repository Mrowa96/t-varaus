import { initialState } from '../store/initialState';
import { ADD_REQUEST, UPDATE_REQUEST } from '../actions/requests';
import RequestStatus from '../../models/RequestStatus';

export function requests(state = initialState.requests, action) {
  let request;

  switch (action.type) {
    case ADD_REQUEST:
      return {
        ...state,
        [action.payload.requestId]: {
          method: action.payload.method,
          endpoint: action.payload.endpoint,
          filters: action.payload.filters,
          data: action.payload.data,
          status: RequestStatus.PENDING,
        },
      };

    case UPDATE_REQUEST:
      request = {
        ...state[action.payload.requestId],
      };

      Object.keys(action.payload.data).forEach(key => {
        if (Object.prototype.hasOwnProperty.call(action.payload.data, key)) {
          request[key] = action.payload.data[key];
        }
      });

      return {
        ...state,
        [action.payload.requestId]: request,
      };

    default:
      return state;
  }
}
