import { initialState } from '../store/initialState';
import { arrayToObject } from '../../utils/arrayToObject';
import { FILTERED_NEWS_FETCHED, NEWS_NOT_FOUND, ONE_NEWS_FETCHED } from '../actions/news';
import { serializeFilters } from '../../utils/serializeFilters';

export function news(state = initialState.news, action) {
  let newsAsObject;

  switch (action.type) {
    case FILTERED_NEWS_FETCHED:
      newsAsObject = arrayToObject(action.payload.response.data);

      return {
        ...state,
        byId: {
          ...state.byId,
          ...newsAsObject,
        },
        dataByFilter: {
          ...state.dataByFilter,
          [serializeFilters(action.payload.request.filters)]: {
            ids: Object.keys(newsAsObject).map(id => +id),
            totalQuantity: +action.payload.response.headers['x-total-count'],
          },
        },
      };

    case ONE_NEWS_FETCHED:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.request.filters.id]: action.payload.response.data,
        },
      };

    case NEWS_NOT_FOUND:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.request.filters.id]: {},
        },
      };

    default:
      return state;
  }
}
