import { combineReducers } from 'redux';
import { events } from './events';
import { requests } from './requests';
import { places } from './places';
import { tickets } from './tickets';
import { reservation } from './reservation';
import { news } from './news';

const reducers = combineReducers({
  events,
  news,
  places,
  tickets,
  reservation,
  requests,
});

export default reducers;
