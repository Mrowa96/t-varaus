import { initialState } from '../store/initialState';
import { PLACES_FETCHED } from '../actions/places';
import { arrayToObject } from '../../utils/arrayToObject';

export function places(state = initialState.places, action) {
  let placesAsObject;

  switch (action.type) {
    case PLACES_FETCHED:
      placesAsObject = arrayToObject(action.payload.response.data);

      return {
        ...state,
        byId: {
          ...state.byId,
          ...placesAsObject,
        },
      };

    default:
      return state;
  }
}
