export const initialState = {
  events: {
    byId: {},
    dataByFilter: {},
    lastSeenIds: [],
    filters: {
      placeId: null,
      date: null,
    },
  },
  news: {
    byId: {},
    dataByFilter: {},
  },
  places: {
    byId: null,
  },
  tickets: {
    byId: null,
  },
  reservation: {
    id: null,
    eventId: null,
    showId: null,
    place: null,
    date: null,
    startTime: null,
    tickets: [],
    seats: [],
    takenSeats: [],
    agreements: {
      dataProcessing: false,
    },
    clientData: {
      name: '',
      surname: '',
      email: '',
      phone: '',
    },
    lastVisitedStep: null,
    fromStorage: false,
  },
  requests: {},
};
