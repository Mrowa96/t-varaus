import moment from 'moment';

export default function isValid(reservation, currentDateTime = moment().format('YYYY-MM-DD HH:mm')) {
  const hasBasicParameters = !!(
    reservation.id &&
    reservation.eventId &&
    reservation.showId &&
    reservation.place &&
    reservation.date &&
    reservation.startTime
  );

  if (hasBasicParameters) {
    const showDateTime = moment(`${reservation.date} ${reservation.startTime}`, 'YYYY-MM-DD HH:mm').subtract(
      30,
      'minutes',
    );

    if (showDateTime.isSameOrAfter(currentDateTime)) {
      return true;
    }
  }

  return false;
}
