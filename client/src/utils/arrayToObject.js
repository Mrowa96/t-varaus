export function arrayToObject(arr, key = 'id') {
  return arr.reduce((acc, cur) => ({ ...acc, [cur[key]]: cur }), {});
}
