export function getRouteByReservationStep(routes, lastVisitedStep) {
  return Object.values(routes).find(route => route.data && route.data.reservationStep === lastVisitedStep);
}
