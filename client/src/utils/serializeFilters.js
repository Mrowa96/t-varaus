export function serializeFilters(filters) {
  let serializedFilter = 'f_';

  if (filters) {
    serializedFilter = Object.entries(filters).reduce(
      (computedValue, [key, value]) => `${computedValue}${key}_${value}_`,
      serializedFilter,
    );
  }

  return serializedFilter;
}
