process.env.BABEL_ENV = 'production';
process.env.NODE_ENV = 'production';

process.on('unhandledRejection', (err) => {
  throw err;
});

const chalk = require('chalk');
const webpack = require('webpack');
const config = require('../config/webpack.config');

console.log(chalk.blue('[SERVER] Creating production build...\n'));

const compiler = webpack(config);

compiler.run((err, stats) => {
  console.log(
    stats.toString({
      assets: false,
      children: false,
      entrypoints: false,
      chunks: false,
      colors: true,
      performance: false,
      usedExports: false,
      modules: false,
    }),
  );
});
