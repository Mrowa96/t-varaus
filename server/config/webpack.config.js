const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const getCSSModuleLocalIdent = require('react-dev-utils/getCSSModuleLocalIdent');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const getClientEnvironment = require('../../client/config/env');

const cssRegex = /\.css$/;
const cssModuleRegex = /\.module\.css$/;
const sassRegex = /\.(scss|sass)$/;
const sassModuleRegex = /\.module\.(scss|sass)$/;

const env = getClientEnvironment('');

const getStyleLoaders = (cssOptions, preProcessor) => {
  const loaders = [
    {
      loader: MiniCssExtractPlugin.loader,
    },
    {
      loader: require.resolve('css-loader'),
      options: cssOptions,
    },
  ];

  if (preProcessor) {
    loaders.push(require.resolve(preProcessor));
  }

  return loaders;
};

module.exports = {
  entry: './server/src/index.js',
  target: 'node',
  mode: process.env.NODE_ENV,
  externals: [nodeExternals()],
  output: {
    path: path.resolve('./server/build'),
    filename: 'index.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: require.resolve('babel-loader'),
      },
      {
        test: cssRegex,
        exclude: cssModuleRegex,
        use: getStyleLoaders({
          importLoaders: 1,
        }),
      },
      {
        test: cssModuleRegex,
        use: getStyleLoaders({
          importLoaders: 1,
          modules: true,
          getLocalIdent: getCSSModuleLocalIdent,
        }),
      },
      {
        test: sassRegex,
        exclude: sassModuleRegex,
        use: getStyleLoaders({ importLoaders: 2 }, 'sass-loader'),
      },
      {
        test: sassModuleRegex,
        use: getStyleLoaders(
          {
            importLoaders: 2,
            modules: true,
            getLocalIdent: getCSSModuleLocalIdent,
          },
          'sass-loader',
        ),
      },
      {
        test: /\.(ttf|eot|otf|svg|png)$/,
        loader: 'file-loader?emitFile=false',
      },
      {
        test: /\.(woff|woff2)$/,
        loader: 'url-loader?emitFile=false',
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin(env.stringified),
    new webpack.ProvidePlugin({
      URLSearchParams: ['url', 'URLSearchParams'],
    }),
    new MiniCssExtractPlugin(),
  ],
};
