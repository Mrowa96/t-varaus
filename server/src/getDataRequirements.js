import { matchPath } from 'react-router-dom';
import url from 'url';
import routes from '../../client/src/routes';

export default function getDataRequirements(store, request) {
  const actions = [];

  Object.values(routes).forEach(route => {
    const matchedRoute = matchPath(url.parse(request.url).pathname, route);

    if (matchedRoute && route.component) {
      const { fetchInitialData } = route.component;

      if (fetchInitialData) {
        fetchInitialData({ query: request.query, params: matchedRoute.params }).forEach(action => {
          actions.push(store.dispatch(action));
        });
      }

      return 0;
    }
  });

  return actions;
}
