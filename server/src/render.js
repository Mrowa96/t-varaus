import path from 'path';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import Helmet from 'react-helmet';
import fs from 'fs';
import React from 'react';
import App from '../../client/src/components/App/App';

export default function render(store, request, response) {
  const indexFile = path.resolve('./client/build/index.html');
  const state = store.getState();
  const reactApp = renderToString(
    <Provider store={store}>
      <StaticRouter context={{}} location={request.url}>
        <App />
      </StaticRouter>
    </Provider>,
  );
  const helmetData = Helmet.renderStatic();

  fs.readFile(indexFile, 'utf8', (error, data) => {
    if (error) {
      console.error('Something went wrong: ', error);

      return response.status(500).send(error);
    }

    data = data.replace('<title/>', `${helmetData.title.toString()}`);
    data = data.replace('<meta/>', `${helmetData.meta.toString()}`);
    data = data.replace(
      '<div id="app"></div>',
      `<div id="app">${reactApp}</div>
        <script id="initialData">window.__INITIAL_DATA__ = ${JSON.stringify(state)}</script>`,
    );

    return response.send(data);
  });
}
