import * as express from 'express';
import compression from 'compression';
import configureStore from './configureStore';
import getDataRequirements from './getDataRequirements';
import render from './render';

const PORT = process.env.PORT || 3307;
const app = express();

global.navigator = {
  geolocation: {},
};

if (process.env.NODE_ENV === 'production') {
  app.use(compression());
}

app.get('/report.html', (request, response) => {
  response.status(404).send('Page not found');
});

app.use(express.static('./client/build', { index: false, extensions: ['html'] }));

app.get('/*', (request, response) => {
  const store = configureStore();
  const dataRequirements = getDataRequirements(store, request);

  if (dataRequirements.length) {
    Promise.all(dataRequirements);
  } else {
    return render(store, request, response);
  }

  const unsubscribe = store.subscribe(() => {
    const state = store.getState();
    const { requests } = state;

    if (
      !Object.getOwnPropertySymbols(requests)
        .map(s => requests[s].status)
        .includes('PENDING')
    ) {
      unsubscribe();

      return render(store, request, response);
    }
  });
});

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});
