# Sinehan app

## Description
App displaying cinema movies and making reservations

## Requirements
- node >= 8.12.0
- npm >= 6.4.1

Additional requirements for development
- [sinehan-api](https://gitlab.com/Mrowa96/sinehan-api)
- [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
- LF line endings

## Commands
- `npm run client:build` - build client package optimized for production
- `npm run client:build:dev` - build client package used for development with watcher
- `npm run client:test` - test client files and exit after finish
- `npm run client:test:dev` - test client files and enable interactive console
- `npm run client:lint` - run linter for client code
- `npm run server:build` - build server package optimized for production
- `npm run server:build:dev` - build server package used for development with watcher
- `npm run server:start` - run build-in http server, only for development
- `npm run server:lint` - run linter for server code

## Run development environment
- copy `.env.dist` to `.env` and customize it for ow purposes
- run `npm install`
- run `npm run client:build:dev`
- run `npm run server:build:dev`
- run `npm run server:start`

The first two commands have to be executed only once.

## Browsers support
- Last two versions of Chrome, Firefox, Opera, Edge, Safari, Samsung Browser
- IE is not supported (desktop and mobile), Opera Mini as well
