# Changelog

## 1.0.0
- First release

## 1.1.0
#### Features:
- Add some e2e tests (#40)
- Rewrite store (#32)
- Write more unit tests (#38, #11)
- Add real api connection (#17)
- Simplify containers which makes requests (#42)

#### Bugfixes:
- Fix problem with glitchy header (#46)
- Fix production build (#45)
- Fix production details on event detail page (#27)
- Fix infinite loop when event or news does not exists (#23)
- Fix handling date in FilteredEventsList (#43)

## 1.1.1 and 1.1.2
- Change version in package.json and package.lock

## 1.2.0
#### Features:
- Update babel version to 7.2 and throw out babel-preset-react-app (#48)
- Extend reservation process with auto-save feature and enable to save reservation in database(#34, #56, #55, #53, #52, #66)
- Update react to 16.7.0 (#57)

## 1.3.0
#### Feature:
- Reduce seats information in reservation quick summary (#68)
- Disable hover on mobile (#5)
- Move config to .env (#61)
- Save state in local storage (#25)
- Move to airbnb lint style (#44)
- Add limit for tickets in reservation (#67)
- Optimize performance (#50)
- Move to https (#80)
- Add Prettier (#77)
#### Bugfixes:
- Fix reservation cost (#71)
- Fix some ugly styles (#75)

## 1.3.1
- Remove source maps from production
